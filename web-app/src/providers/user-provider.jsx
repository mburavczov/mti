import { useContext, createContext } from 'react';
import { useCookies } from 'react-cookie';
import { useRouter } from 'next/router';
import axios from 'axios';
import { BASE_URL } from '../config';
import { useUserInfo } from './use-user-info';
import jwtDecode from 'jwt-decode';

const UserContext = createContext();
export const useUser = () => {
  return useContext(UserContext);
};

export const UserProvider = ({ children }) => {
  const [cookies, setCookie, removeCookie] = useCookies();
  const router = useRouter();

  const isAuth = !!cookies.authJwtToken;
  const role = isAuth ? Number(jwtDecode(cookies.authJwtToken).role) : 0;
  const signIn = (token) => {
    setCookie('authJwtToken', token);
    router.push('/');
  };
  const signOut = () => {
    removeCookie('authJwtToken');
    router.push('/');
  };

  const authHeaders = isAuth
    ? {
        Authorization: `Bearer ${cookies.authJwtToken}`
      }
    : {};
  const { data: user, isLoading: isLoadingUser } = useUserInfo(
    isAuth,
    authHeaders
  );

  const userProps = isAuth
    ? {
        authHeaders,
        user,
        isLoadingUser,
        client: axios.create({
          baseURL: BASE_URL,
          headers: { ...authHeaders }
        })
      }
    : {
        client: axios.create({
          baseURL: BASE_URL
        })
      };

  return (
    <UserContext.Provider value={{ signIn, signOut, isAuth, userProps, role }}>
      {children}
    </UserContext.Provider>
  );
};
