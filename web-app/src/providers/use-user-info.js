import { useQuery } from 'react-query';
import axios from 'axios';
import { BASE_URL } from '../config';

export const useUserInfo = (isAuth, authHeaders) => {
  return useQuery(['user'], async () => {
    if (!isAuth) return {};
    const dto = await axios.get(BASE_URL + 'Account/Info', {
      headers: { ...authHeaders }
    });
    return dto.data;
  });
};
