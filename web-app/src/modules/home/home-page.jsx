import { MainLayout } from '../../components/layout';
import styled from 'styled-components';
import {
    CalendarOutlined,
    BgColorsOutlined,
    WechatOutlined,
    PhoneOutlined,
    SearchOutlined,
    StarOutlined,
    MessageOutlined,
} from '@ant-design/icons';
import { DEVICES } from '../../config';
import { Typography } from 'antd';


export const HomePage = () => {
  return (
    <MainLayout _sider>
        <Typography.Title style={{ textAlign: 'center' }} level={3}>
            Преимущества сервиса
        </Typography.Title>
        <Container>
            <CardWrapper>

                <MainCard>
                    <CardTitle>Мобильное приложение</CardTitle>
                    <IconWrapper>
                        <PhoneOutlined style={{ fontSize: '40px' }} />
                    </IconWrapper>
                    <CardParagraph>Можно использовать приложение с телефона</CardParagraph>
                </MainCard>

                <MainCard>
                    <CardTitle>Нечёткий поиск </CardTitle>
                    <IconWrapper>
                        <SearchOutlined style={{ fontSize: '40px' }} />
                    </IconWrapper>
                    <CardParagraph>Поиск проектов по нечеткому описанию</CardParagraph>
                </MainCard>

                <MainCard>
                    <CardTitle>Сортировка по рейтингу</CardTitle>
                    <IconWrapper>
                        <StarOutlined style={{ fontSize: '40px' }} />
                    </IconWrapper>
                    <CardParagraph>Позволяет отсортировать проекты по рейтнгу</CardParagraph>
                </MainCard>

                <MainCard>
                    <CardTitle>Чат</CardTitle>
                    <IconWrapper>
                        <MessageOutlined style={{ fontSize: '40px' }} />
                    </IconWrapper>
                    <CardParagraph>Возможность общаться в личном чате</CardParagraph>
                </MainCard>
            </CardWrapper>
        </Container>
    </MainLayout>
  );
};


const MainCard = styled.div`
  color: white;
  font-size: 14px;
  background: rgb(26, 115, 232);
  opacity: 0.8;
  max-width: 400px;
  border-radius: 0.5em;
  padding: 1em 2em;
`;

const CardWrapper = styled.div`
  padding: 0 10vw;
  display: flex;
  gap: 4em;
  justify-content: center;
  flex-flow: wrap;
  
 
`;

const CardTitle = styled.h3`
  color: white;
  text-align: center;
  font-size: 2em;
`;
const CardParagraph = styled.p`
  margin-top: 0.5em;
  font-size: 1.5em;
  text-align: center;
`;
const IconWrapper = styled.div`
  width: 40px;
  margin: auto;
`;
const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: calc(100% - 30px);
`;

