import { MainLayout } from '../../components/layout';
import { Collapse, Typography } from 'antd';
import { CaretRightOutlined } from '@ant-design/icons';
import { RequestTable } from './request-table';
import { useGetProblems } from './api/use-get-problems';
import { RequestCreateChat } from './request-create-chat';

export const RequestsOnSearchPage = () => {
  const { data: problems } = useGetProblems();

  return (
    <MainLayout _sider>
      <Typography.Title level={2}>Заявки на поиск проекта</Typography.Title>{' '}
      <Collapse
        style={{ marginTop: '10px' }}
        bordered={false}
        expandIcon={({ isActive }) => (
          <CaretRightOutlined rotate={isActive ? 90 : 0} />
        )}
        className="site-collapse-custom-collapse"
      >
        {problems &&
          problems.length !== 0 &&
          problems.map((problem) => (
            <Collapse.Panel
              key={problem.id}
              header={problem.shortDescription}
              className="site-collapse-custom-panel"
            >
              <RequestCreateChat withUser={problem.personId} />
              <RequestTable problem={problem} />
            </Collapse.Panel>
          ))}
      </Collapse>
    </MainLayout>
  );
};
