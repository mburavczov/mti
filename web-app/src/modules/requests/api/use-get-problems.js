import { useUser } from '../../../providers';
import { useQuery } from 'react-query';

export const useGetProblems = () => {
  const { userProps } = useUser();
  const { client } = userProps;
  return useQuery(['problems'], async () => {
    const dto = await client.get(`Project/GetProblems`);
    return dto.data;
  });
};
