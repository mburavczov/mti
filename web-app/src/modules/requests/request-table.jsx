import { Table } from 'antd';

export const RequestTable = ({ problem }) => {
  const dataSource = [
    {
      key: 0,
      name: 'Что болит?',
      value: problem.shortDescription
    },
    {
      key: 1,
      name: 'Как проявляется ваша проблема?',
      value: problem.longDescription
    },
    {
      key: 2,
      name: 'Что будет если проблему не решать?',
      value: problem.aftereffect
    },
    {
      key: 3,
      name: 'Почему так происходит?',
      value: problem.reason
    },

    {
      key: 4,
      name: 'У кого болит?',
      value: problem.affectedTo
    },
    {
      key: 5,
      name: 'Какой желательный срок решения проблемы?',
      value: problem.period
    },
    {
      key: 6,
      name: 'Пробовали решать?',
      value: problem.attemptToSolve
    },
    {
      key: 7,
      name: 'Как с вами связаться?',
      value: problem.contacts
    }
  ];
  const columns = [
    {
      title: 'Поле',
      dataIndex: 'name',
      key: 'name',
      width: '35%'
    },
    {
      title: 'Значение',
      dataIndex: 'value',
      key: 'value',
      width: '65%'
    }
  ];
  return (
    <Table
      dataSource={dataSource}
      columns={columns}
      pagination={{ defaultPageSize: 20, hideOnSinglePage: true }}
      scroll={{ x: true }}
    />
  );
};
