import { useUser } from '../../../providers';
import { useMutation } from 'react-query';

export const useCreateChat = () => {
  const { userProps } = useUser();
  const { client } = userProps;

  return useMutation(async ({ personIds, name }) => {
    await client.post('Chat/CreateChat', {
      personIds,
      name
    });
  });
};
