import { useUser } from '../../../providers';
import { useQuery } from 'react-query';

export const useGetChatMessages = (chatId) => {
  const { userProps } = useUser();
  const { client } = userProps;
  return useQuery(
    ['chat-messages'],
    async () => {
      const dto = await client.get('Chat/ChatMessages', {
        params: { chatId }
      });
      return dto.data;
    },
    {
      refetchInterval: 3000
    }
  );
};
