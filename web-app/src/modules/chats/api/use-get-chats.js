import { useUser } from '../../../providers';
import { useQuery } from 'react-query';

export const useGetChats = () => {
  const { userProps } = useUser();
  const { client } = userProps;
  return useQuery(['chats'], async () => {
    const dto = await client.get('Chat/Chats');
    return dto.data;
  });
};
