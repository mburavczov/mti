import { useUser } from '../../../providers';
import { useMutation } from 'react-query';

export const useSendMessage = () => {
  const { userProps } = useUser();
  const { client } = userProps;

  return useMutation(async ({ chatId, message }) => {
    await client.post('Chat/SendMessage', {
      chatId,
      text: message
    });
  });
};
