import { Button, Input } from 'antd';
import { SendOutlined } from '@ant-design/icons';
import { useState } from 'react';
import { useSendMessage } from '../api/use-send-message';
import { useQueryClient } from 'react-query';

export const SendMessage = ({ chatId }) => {
  const queryClient = useQueryClient();
  const [message, setMessage] = useState('');
  const { mutate: onSendMessage } = useSendMessage();
  const sendMessage = async () => {
    if (message === '') return null;
    await onSendMessage({ chatId, message });
    await queryClient.invalidateQueries('chat-messages');
    setMessage('');
  };

  return (
    <Input.Group style={{ marginTop: '20px' }} compact>
      <Input
        value={message}
        onChange={(event) => setMessage(event.target.value)}
        style={{ width: 'calc(100% - 46px)' }}
        placeholder={'Сообщение'}
        onKeyPress={async (event) => {
          if (event.key === 'Enter') {
            await sendMessage();
          }
        }}
      />
      <Button type="primary" onClick={sendMessage}>
        <SendOutlined />
      </Button>
    </Input.Group>
  );
};
