import { Comment, Tooltip } from 'antd';
import moment from 'moment';
import styled from 'styled-components';

export const ChatMessages = ({ messages }) => {
  return (
    <Container>
      {messages.map((message) => (
        <Comment
          style={{ direction: 'ltr', transform: 'rotate(180deg)' }}
          key={message.text + message.createdAt}
          author={message.senderName}
          content={<p>{message.text}</p>}
          datetime={
            <Tooltip title={moment(message.createdAt).format('YYYY-MM-DD HH:mm:ss')}>
              <span>{moment(message.createdAt).format('YYYY-MM-DD HH:mm ')}</span>
            </Tooltip>
          }
        />
      ))}
    </Container>
  );
};

const Container = styled.div`
  max-height: calc(100vh - 300px);
  overflow-y: scroll;
  direction: rtl;
  transform: rotate(180deg);
  ::-webkit-scrollbar {
    width: 0;
  }
`;
