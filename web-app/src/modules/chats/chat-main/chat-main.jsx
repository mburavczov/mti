import { Center } from '../../../components/layout';
import { Typography } from 'antd';
import { BaseChat } from './base-chat';

export const ChatMain = () => {
  return (
    <Center _alignItems={false}>
      <Typography.Title style={{ textAlign: 'center' }} level={3}>
        Чат
      </Typography.Title>
      <BaseChat />
    </Center>
  );
};
