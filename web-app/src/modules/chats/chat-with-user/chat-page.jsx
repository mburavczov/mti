import { MainLayout } from '../../../components/layout';
import { ChatMain } from '../chat-main/chat-main';

export const ChatPage = () => {
  return (
    <MainLayout _sider>
      <ChatMain />
    </MainLayout>
  );
};
