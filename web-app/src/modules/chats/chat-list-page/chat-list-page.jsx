import { MainLayout } from '../../../components/layout';
import { ChatListPageMain } from './chat-list-page-main';

export const ChatListPage = () => {
  return (
    <MainLayout _sider>
      <ChatListPageMain />
    </MainLayout>
  );
};
