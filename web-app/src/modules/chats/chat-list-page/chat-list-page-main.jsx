import { Typography } from 'antd';
import { ChatsList } from './chats-list';
import { useGetChats } from '../api/use-get-chats';

export const ChatListPageMain = () => {
  const { data, isLoading } = useGetChats();

  if ((!data || !data.chats) && isLoading) {
    return <Typography.Title level={3}>Чаты</Typography.Title>;
  }

  if (!data || !data.chats) {
    return (
      <>
        <Typography.Title level={3}>Чаты</Typography.Title>
      </>
    );
  }

  return (
    <>
      <Typography.Title level={3}>Чаты</Typography.Title>
      <ChatsList chats={data.chats} />
    </>
  );
};
