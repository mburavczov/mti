import { Button, Card } from 'antd';
import Link from 'next/link';

export const ChatsList = ({ chats }) => {
  if (!chats || chats.length === 0) return null;

  return (
    <>
      {chats.map((chat) => (
        <Card
          key={chat.id}
          style={{ marginTop: 16 }}
          type="inner"
          title={chat.name}
          extra={
            <Button type="link">
              <Link href={`/chats/${chat.id}`}>Чат</Link>
            </Button>
          }
        >
          <b>Участники:</b>
          <ul>
            {chat.persons.map((person) => (
              <li key={person.name}>{person.name}</li>
            ))}
          </ul>
        </Card>
      ))}
    </>
  );
};
