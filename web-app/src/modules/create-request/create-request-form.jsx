import { Button, Form, Input, Typography } from 'antd';
import { useCreateRequest } from './api/use-create-request';
import styled from 'styled-components';
import { Success } from '../../components/errors';

export const CreateRequestForm = () => {
  const { mutate: onSignUp, isError, isSuccess } = useCreateRequest();
  if (isSuccess) return <Success />;
  return (
    <Form name="auth" layout={'vertical'} onFinish={onSignUp}>
      <TitleContainer>
        <Typography.Title level={3}>Ваша проблема</Typography.Title>
        <Typography.Title level={5} type="danger">
          {isError ? 'Что-то пошло не так...' : ''}
        </Typography.Title>
      </TitleContainer>

      <Form.Item
        label="Что болит?"
        name="shortDescription"
        rules={[
          {
            required: true,
            message: 'Данной поле должно быть заполнено'
          }
        ]}
      >
        <Input.TextArea
          autoSize
          placeholder="Опишите своими словами существующую в организации проблему. Можно сформулировать проблему в форму задачи"
        />
      </Form.Item>
      <Form.Item label="Как проявляется ваша проблема?" name="longDescription">
        <Input.TextArea
          autoSize
          placeholder="Приведите описание реальной ситуации, в которой проблема бы проявилась"
        />
      </Form.Item>
      <Form.Item label="Что будет если проблему не решать?" name="aftereffect">
        <Input.TextArea
          autoSize
          placeholder="Опишите нежелательные эффекты, которые возникают или могут возникнуть из-за того, что проблема не решается"
        />
      </Form.Item>
      <Form.Item label="Почему так происходит?" name="reason">
        <Input.TextArea
          autoSize
          placeholder="Какие на ваш взгляд ключевые причины возникновения проблемы? Что на ваш взгляд является причиной возникновения проблемы?"
        />
      </Form.Item>
      <Form.Item label="У кого болит?" name="affectedTo">
        <Input.TextArea
          autoSize
          placeholder="Кто является непосредственно ответственным за проблемный участок?"
        />
      </Form.Item>
      <Form.Item label="Какой желательный срок решения проблемы?" name="period">
        <Input.TextArea
          autoSize
          placeholder="Какой желательный срок решения проблемы?"
        />
      </Form.Item>
      <Form.Item label="Пробовали решать?" name="attemptToSolve">
        <Input.TextArea
          autoSize
          placeholder="Как пытались решить проблему ранее? Почему эти попытки оказались неудачными или почему были признаны неудачными? Чем не устроили найденные решения?
Общались ли с рынком? Если да, то с кем?"
        />
      </Form.Item>
      <Form.Item
        label="Как с вами связаться?"
        name="contacts"
        rules={[
          {
            required: true,
            message: 'Данной поле должно быть заполнено'
          }
        ]}
      >
        <Input.TextArea
          autoSize
          placeholder="Укажите наименование вашего предприятия, ваши ФИО, и телефон для связи"
        />
      </Form.Item>
      <Form.Item key="submit">
        <Button style={{ width: '100%' }} type="primary" htmlType="submit">
          Добавить проблему
        </Button>
      </Form.Item>
    </Form>
  );
};

const TitleContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;
