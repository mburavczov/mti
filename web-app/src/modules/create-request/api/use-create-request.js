import { useUser } from '../../../providers';
import { useMutation } from 'react-query';

export const useCreateRequest = () => {
  const { userProps } = useUser();
  const { client } = userProps;

  return useMutation(
    async (body) => await client.post('Project/AddProblem', body)
  );
};
