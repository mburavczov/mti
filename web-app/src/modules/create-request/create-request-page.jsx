import { Center, MainLayout } from '../../components/layout';
import { useUser } from '../../providers';
import { USER_SELLER } from '../../config';
import { AccessDenied } from '../../components/errors/access-denied';
import { CreateRequestForm } from './create-request-form';

export const CreateRequestPage = () => {
  const { role } = useUser();
  if (role === USER_SELLER || role === 0) return <AccessDenied />;

  return (
    <MainLayout _sider>
      <Center _alignItems={false}>
        <CreateRequestForm />
      </Center>
    </MainLayout>
  );
};
