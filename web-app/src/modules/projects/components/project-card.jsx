import { Rate, Table } from 'antd';

export const ProjectCard = ({ project }) => {
  const dataSource = [
    {
      key: 0,
      name: 'Наименование команды/организации',
      value: project.authorName
    },
    {
      key: 1,
      name: 'Стадия готовности продукта',
      value: project.status
    },
    {
      key: 2,
      name: 'Краткое описание продукта',
      value: project.shortDescription
    },
    {
      key: 3,
      name: 'Кейсы использования продукта',
      value: project.useCase
    },

    {
      key: 4,
      name: 'Польза продукта',
      value: project.benefits
    },
    {
      key: 5,
      name: 'Организация Московского транспорта, интересная в первую очередь',
      value: project.targetOrganization
    },
    {
      key: 6,
      name: 'Запрос к акселератору и видение пилотного проекта',
      value: project.vision
    },
    {
      key: 7,
      name: 'Требуется ли сертификация продукта',
      value: project.needCertification
    },
    {
      key: 15,
      name: 'Сайт',
      value: project.site
    },
    {
      key: 17,
      name: 'Ссылка на презентацию',
      value: project.presentationLink
    },
    {
      key: 21,
      name: 'Рейтинг',
      value: <Rate disabled value={project.rating} />
    }
  ];
  const columns = [
    {
      title: 'Поле',
      dataIndex: 'name',
      key: 'name',
      width: '35%'
    },
    {
      title: 'Значение',
      dataIndex: 'value',
      key: 'value',
      width: '65%'
    }
  ];
  return (
    <Table
      dataSource={dataSource}
      columns={columns}
      pagination={{ defaultPageSize: 20, hideOnSinglePage: true }}
      scroll={{ x: true }}
    />
  );
};
