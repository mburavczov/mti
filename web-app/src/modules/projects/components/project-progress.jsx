import { Rate, Steps } from 'antd';
import { STATES_PROJECTS } from '../../../config';

const { Step } = Steps;

export const ProjectProgress = ({ project }) => {
  return (
    <Steps progressDot current={project.state} direction="vertical">
      {STATES_PROJECTS.map(([index, name]) => (
        <Step
          title={name}
          description={
            index !== 5 && (
              <Rate disabled value={project.scoreHistories[index]?.value} />
            )
          }
        />
      ))}
    </Steps>
  );
};
