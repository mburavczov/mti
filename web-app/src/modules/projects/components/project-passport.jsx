import { Collapse, Rate, Table } from 'antd';
import { CaretRightOutlined } from '@ant-design/icons';

export const ProjectPassport = ({ project }) => {
  const dataSource = [
    {
      key: 0,
      name: 'Наименование команды/организации',
      value: project.authorName
    },
    {
      key: 1,
      name: 'Стадия готовности продукта',
      value: project.status
    },
    {
      key: 2,
      name: 'Краткое описание продукта',
      value: project.shortDescription
    },
    {
      key: 3,
      name: 'Кейсы использования продукта',
      value: project.useCase
    },

    {
      key: 4,
      name: 'Польза продукта',
      value: project.benefits
    },
    {
      key: 5,
      name: 'Организация Московского транспорта, интересная в первую очередь',
      value: project.targetOrganization
    },
    {
      key: 6,
      name: 'Запрос к акселератору и видение пилотного проекта',
      value: project.vision
    },
    {
      key: 7,
      name: 'Требуется ли сертификация продукта',
      value: project.needCertification
    },
    {
      key: 8,
      name: 'ФИО контактного лица по заявке',
      value: project.contactName
    },
    {
      key: 9,
      name: 'Должность контактного лица',
      value: project.contactPosition
    },
    {
      key: 10,
      name: 'Контактный телефон',
      value: project.contactPhone
    },
    {
      key: 11,
      name: 'Контактная почта',
      value: project.contactEmail
    },
    {
      key: 12,
      name: 'Наименование юридического лица',
      value: project.legalName
    },
    {
      key: 13,
      name: 'ИНН юридического лица',
      value: project.inn
    },
    {
      key: 14,
      name: 'Сколько человек в организации',
      value: project.employeeCount
    },
    {
      key: 15,
      name: 'Сайт',
      value: project.site
    },
    {
      key: 16,
      name: 'Откуда узнали про акселератор',
      value: project.source
    },
    {
      key: 17,
      name: 'Ссылка на презентацию',
      value: project.presentationLink
    },
    {
      key: 21,
      name: 'Рейтинг',
      value: <Rate disabled value={project.rating} />
    }
  ];
  const columns = [
    {
      title: 'Поле',
      dataIndex: 'name',
      key: 'name',
      width: '35%'
    },
    {
      title: 'Значение',
      dataIndex: 'value',
      key: 'value',
      width: '65%'
    }
  ];
  return (
    <Collapse
      style={{ marginTop: '10px' }}
      bordered={false}
      expandIcon={({ isActive }) => (
        <CaretRightOutlined rotate={isActive ? 90 : 0} />
      )}
      className="site-collapse-custom-collapse"
    >
      <Collapse.Panel
        header={'Пасспорт проекта'}
        key={1212}
        className="site-collapse-custom-panel"
      >
        <Table
          dataSource={dataSource}
          columns={columns}
          pagination={{ defaultPageSize: 20, hideOnSinglePage: true }}
          scroll={{ x: true }}
        />
      </Collapse.Panel>
    </Collapse>
  );
};
