import { Select } from 'antd';
import { STATES_PROJECTS } from '../../../config';

export const StateSelect = ({ onChange }) => {
  return (
    <Select onChange={onChange} placeholder={'Выберите статус проекта'}>
      <Select.Option key={111222} value={null}>
        Не выбрано
      </Select.Option>
      {STATES_PROJECTS.map(([index, name]) => (
        <Select.Option key={index} value={index}>
          {name}
        </Select.Option>
      ))}
    </Select>
  );
};
