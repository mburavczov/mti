import { Input } from 'antd';

export const SearchInput = ({ onSearch }) => {
  return (
    <Input.Search
      placeholder="Введите ключевые слова"
      onSearch={onSearch}
      enterButton
    />
  );
};
