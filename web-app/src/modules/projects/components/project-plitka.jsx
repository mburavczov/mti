import { Card, Rate, Typography } from 'antd';
import { useRouter } from 'next/router';
import Link from 'next/link';

export const ProjectPlitka = ({ project }) => {
  const router = useRouter();
  return (
    <Card
      type="inner"
      title={project.authorName}
      extra={
        <Link href={`${router.pathname}/${project.id}`}>
          <a>More</a>
        </Link>
      }
    >
      <Typography.Paragraph>
        <strong>Стадия готовности продукта:</strong> {project.status}
      </Typography.Paragraph>
      <Typography.Paragraph>
        <strong>Краткое описание продукта:</strong> {project.shortDescription}
      </Typography.Paragraph>
      <Typography.Paragraph>
        <strong>Кейсы использования продукта:</strong> {project.useCase}
      </Typography.Paragraph>
      <Typography.Paragraph>
        <strong>Польза продукта:</strong> {project.benefits}
      </Typography.Paragraph>
      <Rate disabled defaultValue={project.rating} />
    </Card>
  );
};
