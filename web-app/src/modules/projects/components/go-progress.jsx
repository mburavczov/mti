import { useGoProgress } from '../api/use-go-progress';
import { DEVICES, INVESTING } from '../../../config';
import { Button, Rate, Typography } from 'antd';
import { useState } from 'react';
import { useQueryClient } from 'react-query';
import styled from 'styled-components';

export const GoProgress = ({ project }) => {
  const { mutate: goProgress } = useGoProgress();
  const queryClient = useQueryClient();
  const [rate, setRate] = useState(5);
  const nextState = () => {
    const body = {
      projectId: project.id,
      score: rate
    };
    goProgress(body, {
      onSuccess: async () => {
        await queryClient.invalidateQueries(['project']);
      }
    });
  };
  if (project.state === INVESTING[0]) return null;

  return (
    <Container>
      <Typography.Text>Оценка этапа: </Typography.Text>
      <Rate defaultValue={rate} onChange={(value) => setRate(value)} />
      <Button type="primary" onClick={nextState}>
        Следующий этап
      </Button>
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  gap: 10px;
  align-items: center;

  @media ${DEVICES.tablet} {
    flex-direction: column;
  }
`;
