import { ServerErrorPage } from '../../components/errors';
import { useRouter } from 'next/router';
import { useGetProjectById } from './api/use-get-project-by-id';
import { MainLayout } from '../../components/layout';
import { Typography } from 'antd';
import { GoProgress, ProjectPassport, ProjectProgress } from './components';
import styled from 'styled-components';
import { RequestCreateChat } from '../requests';

export const ProjectsCardPage = () => {
  const { query } = useRouter();
  const { data: project, isError } = useGetProjectById(query.id);
  if (isError) return <ServerErrorPage />;

  return (
    <MainLayout _sider>
      <Typography.Title level={2}>
        {project && project.authorName}
      </Typography.Title>
      {project && (
        <Container>
          <RequestCreateChat withUser={project.personId} />
          <ProjectPassport project={project} />
          <GoProgress project={project} />
          <ProjectProgress project={project} />
        </Container>
      )}
    </MainLayout>
  );
};

const Container = styled.div`
  display: flex;
  flex-direction: column;
  gap: 20px;
`;
