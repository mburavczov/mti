import { ServerErrorPage } from '../../components/errors';
import { MainLayout } from '../../components/layout';
import { Typography } from 'antd';
import { ProjectPlitka, SearchInput, StateSelect } from './components';
import { useGetProjectBySearch } from './api/use-get-project-by-search';
import styled from 'styled-components';
import { useState } from 'react';
import { DEVICES } from '../../config';

export const ProjectsPage = () => {
  const [search, setSearch] = useState('');
  const [state, setState] = useState(null);
  const { data: projects, isError } = useGetProjectBySearch(search, state);
  if (isError) return <ServerErrorPage />;

  const onSearch = (value) => setSearch(value);
  const onChange = (value) => setState(value);

  return (
    <MainLayout _sider>
      <Typography.Title level={2}>Проекты</Typography.Title>
      <Container>
        <StateSelect onChange={onChange} />
        <SearchInput onSearch={onSearch} />
      </Container>
      {projects &&
        projects.length !== 0 &&
        projects.map((project) => (
          <ProjectPlitka key={project.id} project={project} />
        ))}
    </MainLayout>
  );
};

const Container = styled.div`
  display: flex;
  gap: 10px;
  margin-bottom: 12px;
  @media ${DEVICES.tablet} {
    flex-direction: column;
  }
`;
