import { ServerErrorPage } from '../../components/errors';
import { useRouter } from 'next/router';
import { useGetProjectById } from './api/use-get-project-by-id';
import { MainLayout } from '../../components/layout';
import { Typography } from 'antd';
import { ProjectCard } from './components';

export const CatalogCardPage = () => {
  const { query } = useRouter();
  const { data: project, isError } = useGetProjectById(query.id);
  if (isError) return <ServerErrorPage />;

  return (
    <MainLayout _sider>
      <Typography.Title level={2}>
        {project && project.authorName}
      </Typography.Title>
      {project && <ProjectCard project={project} />}
    </MainLayout>
  );
};
