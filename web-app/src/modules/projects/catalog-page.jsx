import { ServerErrorPage } from '../../components/errors';
import { MainLayout } from '../../components/layout';
import { Typography } from 'antd';
import { ProjectPlitka, SearchInput } from './components';
import { useGetProjectBySearch } from './api/use-get-project-by-search';
import { useState } from 'react';
import { INVESTING } from '../../config';
import styled from 'styled-components';

export const CatalogPage = () => {
  const [search, setSearch] = useState('');
  const { data: projects, isError } = useGetProjectBySearch(
    search,
    INVESTING[0]
  );
  if (isError) return <ServerErrorPage />;

  const onSearch = (value) => setSearch(value);

  return (
    <MainLayout _sider>
      <Typography.Title level={2}>Каталог проектов</Typography.Title>
      <Container>
        <SearchInput onSearch={onSearch} />
      </Container>
      {projects &&
        projects.length !== 0 &&
        projects.map((project) => (
          <ProjectPlitka key={project.id} project={project} />
        ))}
    </MainLayout>
  );
};

const Container = styled.div`
  margin-bottom: 12px;
`;
