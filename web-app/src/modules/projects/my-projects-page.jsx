import { Typography } from 'antd';
import { MainLayout } from '../../components/layout';
import { useGetUserProjects } from './api/use-get-user-projects';
import { ServerErrorPage } from '../../components/errors';
import { ProjectPlitka } from './components';

export const MyProjectsPage = () => {
  const { data: projects, isError } = useGetUserProjects();
  if (isError) return <ServerErrorPage />;

  return (
    <MainLayout _sider>
      <Typography.Title level={2}>Мои проекты</Typography.Title>
      {projects &&
        projects.length !== 0 &&
        projects.map((project) => (
          <ProjectPlitka key={project.id} project={project} />
        ))}
    </MainLayout>
  );
};
