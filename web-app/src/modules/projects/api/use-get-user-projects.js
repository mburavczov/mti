import { useUser } from '../../../providers';
import { useQuery } from 'react-query';

export const useGetUserProjects = () => {
  const { userProps } = useUser();
  const { client } = userProps;
  return useQuery(['userProjects'], async () => {
    const dto = await client.get(`Project/UserProjects`);
    return dto.data;
  });
};
