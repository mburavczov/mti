import { useQuery } from 'react-query';
import { useUser } from '../../../providers';

export const useGetProjectById = (id) => {
  const { userProps } = useUser();
  const { client } = userProps;
  return useQuery(['project', id], async () => {
    const dto = await client.get(`Project/Projects/${id}`);
    return dto.data;
  });
};
