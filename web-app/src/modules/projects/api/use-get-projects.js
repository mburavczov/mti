import { useUser } from '../../../providers';
import { useQuery } from 'react-query';

export const useGetProjects = (state) => {
  const { userProps } = useUser();
  const { client } = userProps;
  return useQuery(['projects', state], async () => {
    const dto = await client.get(`Project/Projects`, {
      params: { state }
    });
    return dto.data;
  });
};
