import { useUser } from '../../../providers';
import { useQuery } from 'react-query';

export const useGetProjectBySearch = (query, state) => {
  const { userProps } = useUser();
  const { client } = userProps;
  return useQuery(['searchProjects', query, state], async () => {
    const dto = await client.get(`Project/SearchProject`, {
      params: { query, state }
    });
    return dto.data;
  });
};
