import { Center, MainLayout } from '../../../components/layout';
import { SignInForm } from './sign-in-form';
import { useUser } from '../../../providers';
import { IsAuthorize } from '../../../components/errors/is-authorize';

export const SignInPage = () => {
  const { role } = useUser();
  if (role !== 0) return <IsAuthorize />;
  return (
    <MainLayout>
      <Center>
        <SignInForm />
      </Center>
    </MainLayout>
  );
};
