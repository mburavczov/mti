import { Button, Form, Input, Typography } from 'antd';
import styled from 'styled-components';
import { useSignIn } from '../api';
import Link from 'next/link';

export const SignInForm = () => {
  const { mutate: onSignIn, isError } = useSignIn();

  return (
    <Form name="auth" layout={'vertical'} onFinish={onSignIn}>
      <TitleContainer>
        <Typography.Title level={3}>АВТОРИЗАЦИЯ</Typography.Title>
        <Typography.Title level={5} type="danger">
          {isError ? 'Неверный логин или пароль' : ''}
        </Typography.Title>
      </TitleContainer>

      <Form.Item
        label="E-mail"
        name="login"
        rules={[
          {
            required: true,
            message: 'Данной поле должно быть заполнено'
          },
          {
            type: 'email',
            message: 'Введите корректный e-mail'
          }
        ]}
      >
        <Input placeholder={'Введите e-mail'} />
      </Form.Item>
      <Form.Item
        label="Пароль"
        name="pass"
        rules={[
          {
            required: true,
            message: 'Необходимо ввести пароль'
          }
        ]}
      >
        <Input.Password placeholder={'Введите пароль'} />
      </Form.Item>
      <Form.Item key="submit">
        <Button style={{ width: '100%' }} type="primary" htmlType="submit">
          Войти
        </Button>
      </Form.Item>
      <Link href={'/sign-up'}>
        <Button type="link" style={{ width: '100%' }}>
          Регистрация
        </Button>
      </Link>
    </Form>
  );
};

const TitleContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;
