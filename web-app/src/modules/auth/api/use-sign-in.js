import { useUser } from '../../../providers';
import { useMutation } from 'react-query';

export const useSignIn = () => {
  const { signIn, userProps } = useUser();
  const { client } = userProps;

  return useMutation(async (body) => {
    const { data: dto } = await client.post('Account/Token', body);
    signIn(dto.token);
  });
};
