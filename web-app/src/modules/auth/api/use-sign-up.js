import { useUser } from '../../../providers';
import { useMutation } from 'react-query';

export const useSignUp = () => {
  const { signIn, userProps } = useUser();
  const { client } = userProps;

  return useMutation(async (body) => {
    const { data: dto } = await client.post('Account/Register', body);
    signIn(dto.token);
  });
};
