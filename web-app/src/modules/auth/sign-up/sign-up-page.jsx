import { Center, MainLayout } from '../../../components/layout';
import { SignUpForm } from './sign-up-form';
import { useUser } from '../../../providers';
import { IsAuthorize } from '../../../components/errors/is-authorize';

export const SignUpPage = () => {
  const { role } = useUser();
  if (role !== 0) return <IsAuthorize />;
  return (
    <MainLayout>
      <Center>
        <SignUpForm />
      </Center>
    </MainLayout>
  );
};
