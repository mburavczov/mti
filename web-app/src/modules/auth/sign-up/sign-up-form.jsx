import { Button, Form, Input, Typography, Radio } from 'antd';
import styled from 'styled-components';
import { useSignUp } from '../api';
import Link from 'next/link';
import { USER_SELLER, USER_CUSTOMER } from '../../../config';

export const SignUpForm = () => {
  const { mutate: onSignUp, isError } = useSignUp();

  return (
    <Form name="auth" layout={'vertical'} onFinish={onSignUp}>
      <TitleContainer>
        <Typography.Title level={3}>Регистрация</Typography.Title>
        <Typography.Title level={5} type="danger">
          {isError ? 'Что-то пошло не так...' : ''}
        </Typography.Title>
      </TitleContainer>
      <Form.Item
        name="type"
        rules={[
          {
            required: true,
            message: 'Выберите тип аккаунта'
          }
        ]}
      >
        <Radio.Group style={{ width: '100%' }}>
          <ContainerRadioButtons>
            <Radio.Button value={USER_SELLER}>Исполнитель</Radio.Button>
            <Radio.Button value={USER_CUSTOMER}>Заказчик</Radio.Button>
          </ContainerRadioButtons>
        </Radio.Group>
      </Form.Item>
      <Form.Item
        label="Имя"
        name="name"
        rules={[
          {
            required: true,
            message: 'Данной поле должно быть заполнено'
          }
        ]}
      >
        <Input placeholder={'Введите имя'} />
      </Form.Item>
      <Form.Item
        label="Фамилия"
        name="surname"
        rules={[
          {
            required: true,
            message: 'Данной поле должно быть заполнено'
          }
        ]}
      >
        <Input placeholder={'Введите фамилию'} />
      </Form.Item>
      <Form.Item
        label="E-mail"
        name="login"
        rules={[
          {
            required: true,
            message: 'Данной поле должно быть заполнено'
          },
          {
            type: 'email',
            message: 'Введите корректный e-mail'
          }
        ]}
      >
        <Input placeholder={'Введите e-mail'} />
      </Form.Item>
      <Form.Item
        label="Телефон"
        name="phone"
        rules={[
          {
            required: true,
            message: 'Данной поле должно быть заполнено'
          }
        ]}
      >
        <Input placeholder={'Введите номер телефона'} />
      </Form.Item>
      <Form.Item
        label="Пароль"
        name="pass"
        rules={[
          {
            required: true,
            message: 'Необходимо ввести пароль'
          }
        ]}
      >
        <Input.Password placeholder={'Введите пароль'} />
      </Form.Item>
      <Form.Item label="О себе" name="about">
        <Input.TextArea autoSize={true} placeholder="Расскажите о себе" />
      </Form.Item>
      <Form.Item key="submit">
        <Button style={{ width: '100%' }} type="primary" htmlType="submit">
          Зарегистрироваться
        </Button>
      </Form.Item>{' '}
      <Link href={'/sign-in'}>
        <Button type="link" style={{ width: '100%' }}>
          Вход
        </Button>
      </Link>
    </Form>
  );
};

const ContainerRadioButtons = styled.div`
  display: flex;
  width: 100%;
  label {
    width: 50%;
    text-align: center;
  }
`;

const TitleContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;
