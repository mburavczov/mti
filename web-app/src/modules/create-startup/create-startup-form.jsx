import { Button, Form, Input, Select, Typography } from 'antd';
import { useCreateStartup } from './api/use-create-request';
import styled from 'styled-components';
import { Success } from '../../components/errors';

export const CreateStartupForm = () => {
  const { mutate: onSignUp, isError, isSuccess } = useCreateStartup();
  if (isSuccess) return <Success />;
  return (
    <Form name="auth" layout={'vertical'} onFinish={onSignUp}>
      <TitleContainer>
        <Typography.Title level={3}>Ваш проект</Typography.Title>
        <Typography.Title level={5} type="danger">
          {isError ? 'Что-то пошло не так...' : ''}
        </Typography.Title>
      </TitleContainer>
      <Form.Item
        label="Наименование команды/организации"
        name="authorName"
        rules={[
          {
            required: true,
            message: 'Данной поле должно быть заполнено'
          }
        ]}
      >
        <Input placeholder={'Наименование команды/организации'} />
      </Form.Item>
      <Form.Item
        label="Стадия готовности продукта"
        name="status"
        rules={[
          {
            required: true,
            message: 'Данной поле должно быть заполнено'
          }
        ]}
      >
        <Select placeholder={'Стадия готовности продукта'}>
          <Select.Option key="Идея" value="Идея">
            Идея
          </Select.Option>
          <Select.Option key="Прототип" value="Прототип">
            Прототип
          </Select.Option>
          <Select.Option key="Продукт" value="Продукт">
            Продукт
          </Select.Option>
        </Select>
      </Form.Item>
      <Form.Item
        label="Краткое описание продукта"
        name="shortDescription"
        rules={[
          {
            required: true,
            message: 'Данной поле должно быть заполнено'
          }
        ]}
      >
        <Input.TextArea
          autoSize={true}
          placeholder="Краткое описание продукта"
        />
      </Form.Item>
      <Form.Item
        label="Кейсы использования продукта"
        name="useCase"
        rules={[
          {
            required: true,
            message: 'Данной поле должно быть заполнено'
          }
        ]}
      >
        <Input.TextArea
          autoSize={true}
          placeholder="Кейсы использования продукта"
        />
      </Form.Item>
      <Form.Item
        label="Польза продукта"
        name="benefits"
        rules={[
          {
            required: true,
            message: 'Данной поле должно быть заполнено'
          }
        ]}
      >
        <Input.TextArea autoSize={true} placeholder="Польза продукта" />
      </Form.Item>
      <Form.Item
        label="Организация Московского транспорта, интересная в первую очередь"
        name="targetOrganization"
        rules={[
          {
            required: true,
            message: 'Данной поле должно быть заполнено'
          }
        ]}
      >
        <Select
          placeholder={
            'Организация Московского транспорта, интересная в первую очередь'
          }
        >
          <Select.Option key="Метрополитен" value="Метрополитен">
            Метрополитен
          </Select.Option>
          <Select.Option key="Мосгорстранс" value="Мосгорстранс">
            Мосгорстранс
          </Select.Option>
          <Select.Option key="ЦОДД" value="ЦОДД">
            ЦОДД
          </Select.Option>
          <Select.Option
            key="Организатор перевозок"
            value="Организатор перевозок"
          >
            Организатор перевозок
          </Select.Option>
          <Select.Option key="Мостранспроект" value="Мостранспроект">
            Мостранспроект
          </Select.Option>
          <Select.Option key="АМПП" value="АМПП">
            АМПП
          </Select.Option>
        </Select>
      </Form.Item>
      <Form.Item
        label="Запрос к акселератору и видение пилотного проекта"
        name="vision"
        rules={[
          {
            required: true,
            message: 'Данной поле должно быть заполнено'
          }
        ]}
      >
        <Input.TextArea
          autoSize={true}
          placeholder="Запрос к акселератору и видение пилотного проекта"
        />
      </Form.Item>
      <Form.Item
        label="Требуется ли сертификация продукта"
        name="needCertification"
        rules={[
          {
            required: true,
            message: 'Данной поле должно быть заполнено'
          }
        ]}
      >
        <Select placeholder={'Требуется ли сертификация продукта'}>
          <Select.Option
            key="Да, требуется сертификация и у нас она есть"
            value="Да, требуется сертификация и у нас она есть"
          >
            Да, требуется сертификация и у нас она есть
          </Select.Option>
          <Select.Option
            key="Да, требуется сертификация, но  у нас ее нет"
            value="Да, требуется сертификация, но  у нас ее нет"
          >
            Да, требуется сертификация, но у нас ее нет
          </Select.Option>
          <Select.Option key="Нет, не требуется" value="Нет, не требуется">
            Нет, не требуется
          </Select.Option>
        </Select>
      </Form.Item>
      <Form.Item
        label="ФИО контактного лица по заявке"
        name="contactName"
        rules={[
          {
            required: true,
            message: 'Данной поле должно быть заполнено'
          }
        ]}
      >
        <Input placeholder={'ФИО контактного лица по заявке'} />
      </Form.Item>
      <Form.Item
        label="Должность контактного лица"
        name="contactPosition"
        rules={[
          {
            required: true,
            message: 'Данной поле должно быть заполнено'
          }
        ]}
      >
        <Input placeholder={'Должность контактного лица'} />
      </Form.Item>
      <Form.Item
        label="Контактный телефон"
        name="contactPhone"
        rules={[
          {
            required: true,
            message: 'Данной поле должно быть заполнено'
          }
        ]}
      >
        <Input placeholder={'Контактный телефон'} />
      </Form.Item>
      <Form.Item
        label="Контактная почта"
        name="contactEmail"
        rules={[
          {
            required: true,
            message: 'Данной поле должно быть заполнено'
          },
          {
            type: 'email',
            message: 'Введите корректный e-mail'
          }
        ]}
      >
        <Input placeholder={'Контактная почта'} />
      </Form.Item>
      <Form.Item
        label="Наименование юридического лица"
        name="legalName"
        rules={[
          {
            required: true,
            message: 'Данной поле должно быть заполнено'
          }
        ]}
      >
        <Input placeholder={'Наименование юридического лица'} />
      </Form.Item>
      <Form.Item
        label="ИНН юридического лица"
        name="inn"
        rules={[
          {
            required: true,
            message: 'Данной поле должно быть заполнено'
          }
        ]}
      >
        <Input placeholder={'ИНН юридического лица'} />
      </Form.Item>
      <Form.Item
        label="Сколько человек в организации"
        name="employeeCount"
        rules={[
          {
            required: true,
            message: 'Данной поле должно быть заполнено'
          }
        ]}
      >
        <Select placeholder={'Сколько человек в организации'}>
          <Select.Option key="Менее 20" value="Менее 20">
            Менее 20
          </Select.Option>
          <Select.Option key="от 20 до 100" value="от 20 до 100">
            от 20 до 100
          </Select.Option>
          <Select.Option key="от 100 до 500" value="от 100 до 500">
            от 100 до 500
          </Select.Option>
          <Select.Option key="более 500" value="более 500">
            более 500
          </Select.Option>
        </Select>
      </Form.Item>
      <Form.Item
        label="Сайт"
        name="site"
        rules={[
          {
            required: true,
            message: 'Данной поле должно быть заполнено'
          }
        ]}
      >
        <Input placeholder={'Сайт'} />
      </Form.Item>
      <Form.Item
        label="Откуда узнали про акселератор"
        name="source"
        rules={[
          {
            required: true,
            message: 'Данной поле должно быть заполнено'
          }
        ]}
      >
        <Input placeholder={'Откуда узнали про акселератор'} />
      </Form.Item>
      <Form.Item
        label="Ссылка на презентацию"
        name="presentationLink"
        rules={[
          {
            required: true,
            message: 'Данной поле должно быть заполнено'
          }
        ]}
      >
        <Input placeholder={'Ссылка на презентацию'} />
      </Form.Item>
      <Form.Item key="submit">
        <Button style={{ width: '100%' }} type="primary" htmlType="submit">
          Добавить проект
        </Button>
      </Form.Item>
    </Form>
  );
};

const TitleContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;
