import { Center, MainLayout } from '../../components/layout';
import { useUser } from '../../providers';
import { USER_CUSTOMER } from '../../config';
import { AccessDenied } from '../../components/errors/access-denied';
import { CreateStartupForm } from './create-startup-form';

export const CreateStartupPage = () => {
  const { role } = useUser();
  if (role === USER_CUSTOMER || role === 0) return <AccessDenied />;

  return (
    <MainLayout _sider>
      <Center>
        <CreateStartupForm _alignItems={false} />
      </Center>
    </MainLayout>
  );
};
