import { useUser } from '../../../providers';
import { useMutation } from 'react-query';

export const useCreateStartup = () => {
  const { userProps } = useUser();
  const { client } = userProps;

  return useMutation(
    async (body) => await client.post('Project/AddProject', body)
  );
};
