export const BREAKPOINTS = {
  mobile: '425px',
  tablet: '768px',
  laptop: '1024px',
};

export const DEVICES = {
  mobile: `(max-width: ${BREAKPOINTS.mobile})`,
  tablet: `(max-width: ${BREAKPOINTS.tablet})`,
  laptop: `(max-width: ${BREAKPOINTS.laptop})`,
  desktop: `(min-width: ${BREAKPOINTS.laptop})`,
};
