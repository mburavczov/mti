export const SCREENING = [0, 'Скрининг'];
export const SCORING = [1, 'Скоринг'];
export const EXPERT_COUNCIL = [2, 'Экспертный совет'];
export const ACCELERATION_PROGRAM = [3, 'Акселерационная программа'];
export const PILOTING_PROGRAM = [4, 'Программа пилотирования'];
export const INVESTING = [5, 'Инвестирование'];

export const STATES_PROJECTS = [
  SCREENING,
  SCORING,
  EXPERT_COUNCIL,
  ACCELERATION_PROGRAM,
  PILOTING_PROGRAM,
  INVESTING
];
