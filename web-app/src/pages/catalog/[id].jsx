import { UserProvider } from '../../providers';
import { HeadController } from '../../components/utils/head-controller';
import { CatalogCardPage } from '../../modules/projects';

const CatalogCard = () => {
  return (
    <UserProvider>
      <HeadController title="Решение" />
      <CatalogCardPage />
    </UserProvider>
  );
};

export default CatalogCard;
