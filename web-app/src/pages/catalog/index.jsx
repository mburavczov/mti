import { UserProvider } from '../../providers';
import { HeadController } from '../../components/utils/head-controller';
import { CatalogPage } from '../../modules/projects';

const Catalog = () => {
  return (
    <UserProvider>
      <HeadController title="Каталог" />
      <CatalogPage />
    </UserProvider>
  );
};

export default Catalog;
