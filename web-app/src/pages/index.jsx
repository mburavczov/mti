import { HomePage } from '../modules/home';
import { UserProvider } from '../providers';
import { HeadController } from '../components/utils/head-controller';

const Home = () => {
  return (
    <UserProvider>
      <HeadController title="Главная" />
      <HomePage />
    </UserProvider>
  );
};

export default Home;
