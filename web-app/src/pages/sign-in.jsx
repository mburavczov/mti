import { SignInPage } from '../modules/auth';
import { UserProvider } from '../providers';
import { HeadController } from '../components/utils/head-controller';

const SignIn = () => {
  return (
    <UserProvider>
      <HeadController title="Вход" />
      <SignInPage />
    </UserProvider>
  );
};

export default SignIn;
