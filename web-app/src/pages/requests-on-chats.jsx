import { UserProvider } from '../providers';
import { HeadController } from '../components/utils/head-controller';
import { RequestsOnChatsPage } from '../modules/requests';

const RequestsOnChats = () => {
  return (
    <UserProvider>
      <HeadController title="Заявки на начало чата" />
      <RequestsOnChatsPage />
    </UserProvider>
  );
};

export default RequestsOnChats;
