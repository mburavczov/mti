import { ServerErrorPage } from '../components/errors';
import { UserProvider } from '../providers';
import { HeadController } from '../components/utils/head-controller';

const Error500 = () => {
  return (
    <UserProvider>
      <HeadController title="500" />
      <ServerErrorPage />
    </UserProvider>
  );
};

export default Error500;
