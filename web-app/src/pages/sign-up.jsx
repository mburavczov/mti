import { SignUpPage } from '../modules/auth';
import { UserProvider } from '../providers';
import { HeadController } from '../components/utils/head-controller';

const SignUp = () => {
  return (
    <UserProvider>
      <HeadController title="Регистрация" />
      <SignUpPage />
    </UserProvider>
  );
};

export default SignUp;
