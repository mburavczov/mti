import { UserProvider } from '../providers';
import { HeadController } from '../components/utils/head-controller';
import { ProfilePage } from '../modules/profile';

const Profile = () => {
  return (
    <UserProvider>
      <HeadController title="Мой профиль" />
      <ProfilePage />
    </UserProvider>
  );
};

export default Profile;
