import { AboutPage } from '../modules/home';
import { UserProvider } from '../providers';
import { HeadController } from '../components/utils/head-controller';

const About = () => {
  return (
    <UserProvider>
      <HeadController title="О нас" />
      <AboutPage />
    </UserProvider>
  );
};

export default About;
