import { UserProvider } from '../providers';
import { HeadController } from '../components/utils/head-controller';
import { RequestsOnSearchPage } from '../modules/requests';

const RequestsOnSearch = () => {
  return (
    <UserProvider>
      <HeadController title="Заявки на поиск" />
      <RequestsOnSearchPage />
    </UserProvider>
  );
};

export default RequestsOnSearch;
