import { UserProvider } from '../../providers';
import { HeadController } from '../../components/utils/head-controller';
import { ProjectsPage } from '../../modules/projects';

const Projects = () => {
  return (
    <UserProvider>
      <HeadController title="Проекты" />
      <ProjectsPage />
    </UserProvider>
  );
};

export default Projects;
