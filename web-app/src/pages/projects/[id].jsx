import { UserProvider } from '../../providers';
import { HeadController } from '../../components/utils/head-controller';
import { ProjectsCardPage } from '../../modules/projects';

const ProjectsCard = () => {
  return (
    <UserProvider>
      <HeadController title="Проект" />
      <ProjectsCardPage />
    </UserProvider>
  );
};

export default ProjectsCard;
