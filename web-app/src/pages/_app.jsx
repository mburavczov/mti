import '../styles/globals.css';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';
import { SpinController } from '../components/utils/spin-controller';
import moment from 'moment';
import 'moment/locale/ru';
moment.locale('ru');

export default function MyApp({ Component, pageProps }) {
  const queryClient = new QueryClient();
  return (
    <QueryClientProvider client={queryClient}>
      <SpinController>
        <Component {...pageProps} />
      </SpinController>
      <ReactQueryDevtools />
    </QueryClientProvider>
  );
}
