import { ChatListPage } from '../../modules/chats';
import { UserProvider } from '../../providers';
import { HeadController } from '../../components/utils/head-controller';

const Chats = () => {
  return (
    <UserProvider>
      <HeadController title="Чаты" />
      <ChatListPage />
    </UserProvider>
  );
};

export default Chats;
