import { ChatPage } from '../../modules/chats';
import { UserProvider } from '../../providers';
import { HeadController } from '../../components/utils/head-controller';

const Chat = () => {
  return (
    <UserProvider>
      <HeadController title="Чат" />
      <ChatPage />
    </UserProvider>
  );
};

export default Chat;
