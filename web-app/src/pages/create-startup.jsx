import { UserProvider } from '../providers';
import { HeadController } from '../components/utils/head-controller';
import { CreateStartupPage } from '../modules/create-startup';

const CreateStartup = () => {
  return (
    <UserProvider>
      <HeadController title="Есть проект" />
      <CreateStartupPage />
    </UserProvider>
  );
};

export default CreateStartup;
