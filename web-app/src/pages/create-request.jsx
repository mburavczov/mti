import { UserProvider } from '../providers';
import { HeadController } from '../components/utils/head-controller';
import { CreateRequestPage } from '../modules/create-request';

const CreateRequest = () => {
  return (
    <UserProvider>
      <HeadController title="Нужен проект" />
      <CreateRequestPage />
    </UserProvider>
  );
};

export default CreateRequest;
