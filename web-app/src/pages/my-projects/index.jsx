import { UserProvider } from '../../providers';
import { HeadController } from '../../components/utils/head-controller';
import { MyProjectsPage } from '../../modules/projects';

const MyProjects = () => {
  return (
    <UserProvider>
      <HeadController title="Мои проекты" />
      <MyProjectsPage />
    </UserProvider>
  );
};

export default MyProjects;
