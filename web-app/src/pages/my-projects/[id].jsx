import { UserProvider } from '../../providers';
import { HeadController } from '../../components/utils/head-controller';
import { MyProjectsCardPage } from '../../modules/projects';

const MyProjectCard = () => {
  return (
    <UserProvider>
      <HeadController title="Мой проект" />
      <MyProjectsCardPage />
    </UserProvider>
  );
};

export default MyProjectCard;
