import { NotFoundPage } from '../components/errors';
import { UserProvider } from '../providers';
import { HeadController } from '../components/utils/head-controller';

const Error404 = () => {
  return (
    <UserProvider>
      <HeadController title="404" />
      <NotFoundPage />
    </UserProvider>
  );
};

export default Error404;
