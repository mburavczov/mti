import { Layout } from 'antd';
import { useState } from 'react';
import { Header } from './header';
import { Footer } from './footer';
import { useUser } from '../../providers';
import {
  MenuAdminUser,
  MenuCustomerUser,
  MenuForNoAuthUser,
  MenuSellerUser
} from './menu';
import { ProfileLable } from './profile-lable';
import { USER_ADMIN, USER_CUSTOMER, USER_SELLER } from '../../config';

export const MainLayout = ({ children, _sider = false }) => {
  const [collapsed, setCollapsed] = useState(true);
  const { role } = useUser();

  const getMenu = () => {
    switch (role) {
      case USER_ADMIN:
        return <MenuAdminUser />;
      case USER_CUSTOMER:
        return <MenuCustomerUser />;
      case USER_SELLER:
        return <MenuSellerUser />;
      default:
        return <MenuForNoAuthUser />;
    }
  };
  return (
    <Layout style={{ minHeight: '100vh' }}>
      {_sider && (
        <Layout.Sider
          collapsible
          collapsed={collapsed}
          onCollapse={() => setCollapsed((prev) => !prev)}
        >
          <ProfileLable />
          {getMenu()}
        </Layout.Sider>
      )}
      <Layout>
        <Layout.Header>
          <Header />
        </Layout.Header>
        <Layout.Content style={{ padding: '20px', backgroundColor: 'white' }}>
          {children}
        </Layout.Content>
        <Layout.Footer>
          <Footer />
        </Layout.Footer>
      </Layout>
    </Layout>
  );
};
