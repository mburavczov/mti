import { Typography } from 'antd';
import styled from 'styled-components';

export const Footer = () => {
  return (
    <Container>
      <Typography.Text>Created by team &quot;PHOENIX&quot;</Typography.Text>
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  justify-content: center;
`;
