import styled from 'styled-components';
import { useUser } from '../../providers';
import { Avatar } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import Link from 'next/link';

export const ProfileLable = () => {
  const { isAuth, userProps } = useUser();
  const { user } = userProps;
  const defaultAvatarStyle = { marginRight: '18px', cursor: 'pointer' };

  return (
    <Container>
      {isAuth ? (
        <>
          <Link href={'/profile'} passHref>
            {user && user.avatarPath ? (
              <Avatar style={defaultAvatarStyle} src={user.avatarPath} />
            ) : (
              <Avatar style={defaultAvatarStyle} icon={<UserOutlined />} />
            )}
          </Link>
          <Link href={'/profile'} passHref>
            <a className="ant-layout-sider-collapsed-a">{user && user.name}</a>
          </Link>
        </>
      ) : (
        <>
          <Link href={'/sign-in'} passHref>
            <Avatar style={defaultAvatarStyle} icon={<UserOutlined />} />
          </Link>
          <Link href={'/sign-in'} passHref>
            <a className="ant-layout-sider-collapsed-a">Войти</a>
          </Link>
        </>
      )}
    </Container>
  );
};

const Container = styled.div`
  height: 60px;
  display: flex;
  align-items: center;
  color: white;
  padding-left: 24px;
`;
