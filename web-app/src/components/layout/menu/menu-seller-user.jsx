import { Menu } from 'antd';
import {
  HomeOutlined,
  ImportOutlined,
  MessageOutlined,
  PlusCircleOutlined,
  ProjectOutlined
} from '@ant-design/icons';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useUser } from '../../../providers';

export const MenuSellerUser = () => {
  const router = useRouter();
  const { signOut } = useUser();

  return (
    <Menu theme="dark" defaultSelectedKeys={router.pathname} mode="inline">
      <Menu.Item key="/" icon={<HomeOutlined />}>
        <Link href={'/'}>Главная</Link>
      </Menu.Item>
      <Menu.Item key="/chats" icon={<MessageOutlined />}>
        <Link href={'/chats'}>Чаты</Link>
      </Menu.Item>
      <Menu.Item key="/create-startup" icon={<PlusCircleOutlined />}>
        <Link href={'/create-startup'}>Есть проект</Link>
      </Menu.Item>
      <Menu.Item key="/my-projects" icon={<ProjectOutlined />}>
        <Link href={'/my-projects'}>Мои проекты</Link>
      </Menu.Item>
      <Menu.Item key="5" icon={<ImportOutlined />} onClick={signOut}>
        Выйти
      </Menu.Item>
    </Menu>
  );
};
