import { Menu } from 'antd';
import {
  HomeOutlined,
  ImportOutlined,
  ProjectOutlined,
  ContainerOutlined,
  AppstoreAddOutlined,
  MessageOutlined
} from '@ant-design/icons';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useUser } from '../../../providers';

export const MenuAdminUser = () => {
  const router = useRouter();
  const { signOut } = useUser();

  return (
    <Menu theme="dark" defaultSelectedKeys={router.pathname} mode="inline">
      <Menu.Item key="/" icon={<HomeOutlined />}>
        <Link href={'/'}>Главная</Link>
      </Menu.Item>
      <Menu.Item key="/chats" icon={<MessageOutlined />}>
        <Link href={'/chats'}>Чаты</Link>
      </Menu.Item>
      <Menu.Item key="/projects" icon={<ProjectOutlined />}>
        <Link href={'/projects'}>Проекты</Link>
      </Menu.Item>
      <Menu.Item key="/requests-on-search" icon={<ContainerOutlined />}>
        <Link href={'/requests-on-search'}>Заявки на поиск</Link>
      </Menu.Item>
      <Menu.Item key="/requests-on-chats" icon={<AppstoreAddOutlined />}>
        <Link href={'/requests-on-chats'}>Заявки на чат</Link>
      </Menu.Item>
      <Menu.Item key="5" icon={<ImportOutlined />} onClick={signOut}>
        Выйти
      </Menu.Item>
    </Menu>
  );
};
