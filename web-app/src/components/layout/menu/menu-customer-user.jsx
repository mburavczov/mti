import { Menu } from 'antd';
import {
  HomeOutlined,
  ImportOutlined,
  MessageOutlined,
  PlusCircleOutlined,
  ProjectOutlined
} from '@ant-design/icons';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useUser } from '../../../providers';

export const MenuCustomerUser = () => {
  const router = useRouter();
  const { signOut } = useUser();

  return (
    <Menu theme="dark" defaultSelectedKeys={router.pathname} mode="inline">
      <Menu.Item key="/" icon={<HomeOutlined />}>
        <Link href={'/'}>Главная</Link>
      </Menu.Item>
      <Menu.Item key="/chats" icon={<MessageOutlined />}>
        <Link href={'/chats'}>Чаты</Link>
      </Menu.Item>
      <Menu.Item key="/create-request" icon={<PlusCircleOutlined />}>
        <Link href={'/create-request'}>Нужен проект</Link>
      </Menu.Item>
      <Menu.Item key="/catalog" icon={<ProjectOutlined />}>
        <Link href={'/catalog'}>Каталог</Link>
      </Menu.Item>
      <Menu.Item key="5" icon={<ImportOutlined />} onClick={signOut}>
        Выйти
      </Menu.Item>
    </Menu>
  );
};
