import { Menu } from 'antd';
import {
  HomeOutlined,
  UserAddOutlined,
  InfoCircleOutlined
} from '@ant-design/icons';
import Link from 'next/link';
import { useRouter } from 'next/router';

export const MenuForNoAuthUser = () => {
  const router = useRouter();
  return (
    <Menu theme="dark" defaultSelectedKeys={router.pathname} mode="inline">
      <Menu.Item key="/" icon={<HomeOutlined />}>
        <Link href={'/'}>Главная</Link>
      </Menu.Item>
      <Menu.Item key="/sign-up" icon={<UserAddOutlined />}>
        <Link href={'/sign-up'}>Регистрация</Link>
      </Menu.Item>
      <Menu.Item key="/about" icon={<InfoCircleOutlined />}>
        <Link href={'/about'}>О нас</Link>
      </Menu.Item>
    </Menu>
  );
};
