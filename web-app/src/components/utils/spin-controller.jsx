import { Spin } from 'antd';
import { useRouterRouteChange } from '../../utils';
import { useIsFetching, useIsMutating } from 'react-query';
import { useRouter } from 'next/router';

export const SpinController = ({ children }) => {
  const routerSpin = useRouterRouteChange();
  const isFetching = useIsFetching();
  const isMutating = useIsMutating();
  const router = useRouter();

  return (
    <Spin
      spinning={
        (routerSpin || !!isFetching || !!isMutating) &&
        !!isFetching &&
        router.route !== '/chats/[id]'
      }
      size="large"
    >
      {children}
    </Spin>
  );
};
