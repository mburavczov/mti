import Head from 'next/head';

export const HeadController = ({ title }) => {
  return (
    <Head>
      <title>{title}</title>
    </Head>
  );
};
