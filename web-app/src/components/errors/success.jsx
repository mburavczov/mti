import { Button, Result } from 'antd';
import Link from 'next/link';

export const Success = () => {
  return (
    <Result
      status="success"
      title="Данные успешно отправлены"
      extra={
        <Link href={'/'} passHref>
          <Button type="primary">Главная</Button>
        </Link>
      }
    />
  );
};
