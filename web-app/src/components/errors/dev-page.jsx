import { Result } from 'antd';
import { SmileOutlined } from '@ant-design/icons';
import { Center, MainLayout } from '../layout';

export const DevPage = () => {
  return (
    <MainLayout _sider>
      <Center>
        <Result
          icon={<SmileOutlined />}
          title="Страница находится в разработке"
        />
      </Center>
    </MainLayout>
  );
};
