import { Center, MainLayout } from '../layout';
import { Button, Result } from 'antd';
import Link from 'next/link';

export const AccessDenied = () => {
  return (
    <MainLayout>
      <Center>
        <Result
          status="warning"
          title="Доступ закрыт"
          extra={
            <Link href={'/'} passHref>
              <Button type="primary">Главная</Button>
            </Link>
          }
        />
      </Center>
    </MainLayout>
  );
};
