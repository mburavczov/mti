import { Center, MainLayout } from '../layout';
import { Button, Result } from 'antd';
import Link from 'next/link';

export const NotFoundPage = () => {
  return (
    <MainLayout>
      <Center>
        <Result
          status="404"
          title="404"
          subTitle="Данной страницы не существует."
          extra={
            <Link href={'/'} passHref>
              <Button type="primary">Главная</Button>
            </Link>
          }
        />
      </Center>
    </MainLayout>
  );
};
