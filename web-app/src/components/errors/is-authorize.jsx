import { Center, MainLayout } from '../layout';
import { Button, Result } from 'antd';
import Link from 'next/link';

export const IsAuthorize = () => {
  return (
    <MainLayout>
      <Center>
        <Result
          title="Вы уже авторизованы"
          extra={
            <Link href={'/'} passHref>
              <Button type="primary">Главная</Button>
            </Link>
          }
        />
      </Center>
    </MainLayout>
  );
};
