package ru.nutscoon.mti.services

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query
import ru.nutscoon.mti.models.request.RegisterRequest
import ru.nutscoon.mti.models.request.TokenRequest
import ru.nutscoon.mti.models.response.ChatList
import ru.nutscoon.mti.models.response.ProfileInfo
import ru.nutscoon.mti.models.response.SearchProjects
import ru.nutscoon.mti.models.response.TokenResponse


interface IApiService {
    @POST("account/token")
    fun token(@Body model: TokenRequest): Call<TokenResponse?>
    @POST("account/register")
    fun register(@Body model: RegisterRequest): Call<TokenResponse?>
    @GET("chat/chats")
    fun chats(): Call<ChatList>
    @GET("Project/SearchProject")
    fun searchProjects(@Query("query") query: String): Call<List<SearchProjects>>
}