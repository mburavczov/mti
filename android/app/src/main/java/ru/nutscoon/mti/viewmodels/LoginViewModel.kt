package ru.nutscoon.mti.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import ru.nutscoon.mti.App
import ru.nutscoon.mti.models.request.TokenRequest
import ru.nutscoon.mti.services.IApiService
import ru.nutscoon.mti.services.SharedPreference
import javax.inject.Inject

class LoginViewModel : ViewModel() {

    @Inject
    lateinit var apiService: IApiService
    @Inject
    lateinit var sharedPreference: SharedPreference

    var loginState: MutableLiveData<Boolean> = MutableLiveData()

    init {
        App.component.inject(this)
    }

    fun ready() {
        sharedPreference.removeValue("API_TOKEN")
    }

    fun login(login: String, pass: String){

        val handler = Dispatchers.IO + CoroutineExceptionHandler { _, exception ->
            GlobalScope.launch(Dispatchers.Main) {
                loginState.value = false
            }
        }

        CoroutineScope(handler).launch {
            val response = apiService.token(TokenRequest(login, pass)).execute()
            if (response.isSuccessful) {
                val token = response.body()!!.token
                sharedPreference.save("API_TOKEN", token)

                withContext(Dispatchers.Main){
                    loginState.value = true
                }
            } else {
                withContext(Dispatchers.Main){
                    loginState.value = false
                }
            }
        }
    }
}