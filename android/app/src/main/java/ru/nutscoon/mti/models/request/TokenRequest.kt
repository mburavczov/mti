package ru.nutscoon.mti.models.request

data class TokenRequest(val login: String, val pass: String)
