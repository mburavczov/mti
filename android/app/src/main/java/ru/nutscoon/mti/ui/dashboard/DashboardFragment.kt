package ru.nutscoon.mti.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import ru.nutscoon.mti.databinding.FragmentDashboardBinding

class DashboardFragment : Fragment() {

    private lateinit var viewModel: DashboardViewModel
    private var _binding: FragmentDashboardBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(DashboardViewModel::class.java)

        _binding = FragmentDashboardBinding.inflate(inflater, container, false)
        val root: View = binding.root

        viewModel.errorState.observe(viewLifecycleOwner, {
            if (it) {
                binding.rvProjects.visibility = View.GONE
                binding.tvProjectsErr.visibility = View.VISIBLE
            }
        })
        viewModel.projects.observe(viewLifecycleOwner, {
            if (it.isEmpty()){
                binding.rvProjects.visibility = View.GONE
                binding.tvProjectsErr.visibility = View.VISIBLE
            } else {
                binding.rvProjects.layoutManager = LinearLayoutManager(this.context)
                binding.rvProjects.adapter = RvAdapter(it)
            }
        })
        return root
    }

    override fun onStart() {
        super.onStart()
        viewModel.ready()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}