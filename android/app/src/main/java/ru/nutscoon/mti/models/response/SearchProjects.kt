package ru.nutscoon.mti.models.response

import com.google.gson.annotations.SerializedName


data class SearchProjects(

    @SerializedName("id") var id: Int? = null,
    @SerializedName("personId") var personId: Int? = null,
    @SerializedName("state") var state: Int? = null,
    @SerializedName("authorName") var authorName: String? = null,
    @SerializedName("status") var status: String? = null,
    @SerializedName("shortDescription") var shortDescription: String? = null,
    @SerializedName("useCase") var useCase: String? = null,
    @SerializedName("benefits") var benefits: String? = null,
    @SerializedName("targetOrganization") var targetOrganization: String? = null,
    @SerializedName("vision") var vision: String? = null,
    @SerializedName("needCertification") var needCertification: String? = null,
    @SerializedName("contactName") var contactName: String? = null,
    @SerializedName("contactPosition") var contactPosition: String? = null,
    @SerializedName("contactPhone") var contactPhone: String? = null,
    @SerializedName("contactEmail") var contactEmail: String? = null,
    @SerializedName("legalName") var legalName: String? = null,
    @SerializedName("inn") var inn: String? = null,
    @SerializedName("employeeCount") var employeeCount: String? = null,
    @SerializedName("site") var site: String? = null,
    @SerializedName("rating") var rating: Double? = null

)