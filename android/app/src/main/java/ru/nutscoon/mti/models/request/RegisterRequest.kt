package ru.nutscoon.mti.models.request

data class RegisterRequest(val login: String, val pass: String, val name: String)