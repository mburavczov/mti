package ru.nutscoon.mti

import android.app.Application
import ru.nutscoon.mti.di.ApplicationComponent
import ru.nutscoon.mti.di.ApplicationModule
import ru.nutscoon.mti.di.DaggerApplicationComponent

class App : Application(){
    companion object{
        lateinit var component: ApplicationComponent
    }

    override fun onCreate() {
        super.onCreate()
        initDagger()
    }

    private fun initDagger(){
        component = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .build()
    }
}