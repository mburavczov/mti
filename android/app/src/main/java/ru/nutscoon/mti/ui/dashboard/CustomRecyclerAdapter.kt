package ru.nutscoon.mti.ui.dashboard

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import ru.nutscoon.mti.databinding.ChatItemBinding
import ru.nutscoon.mti.databinding.ProjectItemBinding
import ru.nutscoon.mti.models.response.ChatListItem
import ru.nutscoon.mti.models.response.SearchProjects


class RvAdapter(
    var projects: List<SearchProjects>,
) : RecyclerView.Adapter<RvAdapter.ViewHolder>() {

    inner class ViewHolder(val binding: ProjectItemBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ProjectItemBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder){
            with(projects[position]){

                binding.piTitle.text = this.shortDescription
                binding.piDesc.text = this.benefits
                binding.piState.text = "Статус: ${this.status}"
                binding.piRating.text = "Рейтинг: ${"%.2f".format(this.rating)}"
            }
        }
    }

    override fun getItemCount(): Int {
        return projects.size
    }
}

