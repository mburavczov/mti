package ru.nutscoon.mti.di

import dagger.Component
import ru.nutscoon.mti.services.IApiService
import ru.nutscoon.mti.services.SharedPreference
import ru.nutscoon.mti.ui.chat.ChatViewModel
import ru.nutscoon.mti.ui.dashboard.DashboardViewModel
import ru.nutscoon.mti.ui.home.HomeViewModel
import ru.nutscoon.mti.viewmodels.LoginViewModel
import ru.nutscoon.mti.viewmodels.RegistrationViewModel
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {
    fun getApiService(): IApiService
    fun getSharedPreference(): SharedPreference
    fun inject(viewModel: LoginViewModel)
    fun inject(viewModel: RegistrationViewModel)
    fun inject(viewModel: HomeViewModel)
    fun inject(viewModel: ChatViewModel)
    fun inject(viewModel: DashboardViewModel)
}