package ru.nutscoon.mti.models.response

import com.google.gson.annotations.SerializedName

class ProfileInfo {
    @SerializedName("id")
    var id: Int? = null
    @SerializedName("login")
    var login: String? = null
    @SerializedName("role")
    var role: String? = null
    @SerializedName("state")
    var state: String? = null
    @SerializedName("name")
    var name: String? = null
    @SerializedName("surname")
    var surname: String? = null
    @SerializedName("vk")
    var vk: String? = null
    @SerializedName("phone")
    var phone: String? = null
    @SerializedName("about")
    var about: String? = null
    @SerializedName("avatarPath")
    var avatarPath: String? = null
}
