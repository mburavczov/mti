package ru.nutscoon.mti.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import ru.nutscoon.mti.App
import ru.nutscoon.mti.models.request.RegisterRequest
import ru.nutscoon.mti.services.IApiService
import ru.nutscoon.mti.services.SharedPreference
import javax.inject.Inject

class RegistrationViewModel : ViewModel() {

    @Inject
    lateinit var apiService: IApiService
    @Inject
    lateinit var sharedPreference: SharedPreference

    var regResult: MutableLiveData<Boolean> = MutableLiveData()

    init {
        App.component.inject(this)
    }

    fun registration(login: String, pass: String, name: String){

        val handler = Dispatchers.IO + CoroutineExceptionHandler { _, exception ->
            GlobalScope.launch(Dispatchers.Main) {
                regResult.value = false
            }
        }

        CoroutineScope(handler).launch {
            val response = apiService.register(RegisterRequest(login, pass, name)).execute()
            if (response.isSuccessful) {
                val token = response.body()!!.token
                sharedPreference.save("API_TOKEN", token)

                withContext(Dispatchers.Main){
                    regResult.value = true
                }
            } else {
                withContext(Dispatchers.Main){
                    regResult.value = false
                }
            }
        }
    }
}