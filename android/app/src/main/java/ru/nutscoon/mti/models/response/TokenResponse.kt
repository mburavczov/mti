package ru.nutscoon.mti.models.response

data class TokenResponse(val token: String)