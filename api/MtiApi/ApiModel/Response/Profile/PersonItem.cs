﻿namespace MtiApi.ApiModel.Response.Profile
{
    public class PersonItem
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public int Role { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Phone { get; set; }
        public string About { get; set; }
        public string AvatarPath { get; set; }
    }
}