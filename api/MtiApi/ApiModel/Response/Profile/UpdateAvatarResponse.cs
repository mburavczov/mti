﻿namespace MtiApi.ApiModel.Response.Profile
{
    public class UpdateAvatarResponse
    {
        public string Link { get; set; }
    }
}