﻿using System.Collections.Generic;

namespace MtiApi.ApiModel.Response.Project
{
    public class GetProblemsByPersonIdResponse
    {
        public List<ProblemItem> Problems { get; set; }
    }

    public class ProblemItem
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string Aftereffect { get; set; }
        public string Reason { get; set; }
        public string AffectedTo { get; set; }
        public string Period { get; set; }
        public string AttemptToSolve { get; set; }
        public string Contacts { get; set; }
    }
}