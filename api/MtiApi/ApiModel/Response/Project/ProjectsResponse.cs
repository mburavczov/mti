﻿using System.Collections.Generic;
using MtiApi.Entity;

namespace MtiApi.ApiModel.Response.Project
{
    public class ProjectsResponse
    {
        
    }

    public class ProjectItem
    {
        public int Id { get; set; }
        
        public int PersonId { get; set; }
        public ProjectState State { get; set; }
        
        public string AuthorName { get; set; }
        
        public string Status { get; set; }
        
        public string ShortDescription { get; set; }
        
        public string UseCase { get; set; }
        
        public string Benefits { get; set; }
        public string TargetOrganization { get; set; }
        public string Vision { get; set; }
        public string NeedCertification { get; set; }
        
        public string ContactName { get; set; }
        public string ContactPosition { get; set; }
        
        public string ContactPhone { get; set; }
        
        public string ContactEmail { get; set; }
        public string LegalName { get; set; }
        public string INN { get; set; }
        public string EmployeeCount { get; set; }
        public string Site { get; set; }
        public string Source { get; set; }
        public string PresentationLink { get; set; }
        public double Rating { get; set; }
        public List<ScoreHistoryItem> ScoreHistories { get; set; }
    }

    public class ScoreHistoryItem
    {
        public ProjectState ProjectState { get; set; }
        public double Value { get; set; }
    }
}