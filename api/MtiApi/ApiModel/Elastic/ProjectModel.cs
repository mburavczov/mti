﻿namespace MtiApi.ApiModel.Elastic
{
    public class ProjectModel
    {
        public string Description { get; set; }
        public int ProjectId { get; set; }
    }
}