﻿namespace MtiApi.ApiModel.Elastic
{
    public class ProblemModel
    {
        public string Description { get; set; }
        public int ProblemId { get; set; }
    }
}