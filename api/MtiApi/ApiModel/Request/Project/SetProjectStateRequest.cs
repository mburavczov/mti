﻿using MtiApi.Entity;

namespace MtiApi.ApiModel.Request.Project
{
    public class SetProjectStateRequest
    {
        public int ProjectId { get; set; }
        public ProjectState State { get; set; }
    }
}