﻿namespace MtiApi.ApiModel.Request.Project
{
    public class NextStepRequest
    {
        public int ProjectId { get; set; }
        public double Score { get; set; }
    }
}