﻿using System.Collections.Generic;

namespace MtiApi.ApiModel.Request.Project
{
    public class AddProblemRequest
    {
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string Aftereffect { get; set; }
        public string Reason { get; set; }
        public string AffectedTo { get; set; }
        public string Period { get; set; }
        public string AttemptToSolve { get; set; }
        public string Contacts { get; set; }
        public List<string> Tags { get; set; }
    }
}