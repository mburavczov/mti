﻿using System.Collections.Generic;

namespace MtiApi.ApiModel.Request.Project
{
    public class AddProjectRequest
    {
        public string AuthorName { get; set; }
        
        public string Status { get; set; }
        
        public string ShortDescription { get; set; }
        
        public string UseCase { get; set; }
        
        public string Benefits { get; set; }
        public string TargetOrganization { get; set; }
        public string Vision { get; set; }
        public string NeedCertification { get; set; }
        
        public string ContactName { get; set; }
        public string ContactPosition { get; set; }
        
        public string ContactPhone { get; set; }
        
        public string ContactEmail { get; set; }
        public string LegalName { get; set; }
        public string INN { get; set; }
        public string EmployeeCount { get; set; }
        public string Site { get; set; }
        public string Source { get; set; }
        public string PresentationLink { get; set; }
        
        public List<string> Tags { get; set; }
    }
}