﻿namespace MtiApi.ApiModel.Request.Account
{
    public class RegisterRequest
    {
        public string Login { get; set; }
        public string Pass { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Phone { get; set; }
        public string About { get; set; }
        public PersonType Type { get; set; }
    }

    public enum PersonType
    {
        Customer = 2,
        Seller = 3,
    }
}