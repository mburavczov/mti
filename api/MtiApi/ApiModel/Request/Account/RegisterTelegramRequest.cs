﻿namespace MtiApi.ApiModel.Request.Account
{
    public class RegisterTelegramRequest
    {
        public int PersonId { get; set; }
        public long ChatId { get; set; }
    }
}