﻿namespace MtiApi.ApiModel.Request.Account
{
    public class TokenRequest
    {
        public string Login { get; set; }
        public string Pass { get; set; }
    }
}