﻿namespace MtiApi.ApiModel.Request.Chat
{
    public class CreateChatDto
    {
        public int[] PersonIds { get; set; }
        public string Name { get; set; }
    }
}