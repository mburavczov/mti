﻿namespace MtiApi.Settings
{
    public class Constants
    {
        public const string ClaimTypeUserId = "userId";
        public const string ClaimTypeLogin = "login";
        public const string ClaimTypeRole = "role";
        public const string ClaimTypeName = "name";
    }
}