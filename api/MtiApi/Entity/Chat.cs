﻿using System.Collections.Generic;

namespace MtiApi.Entity
{
    public class Chat
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
        public virtual ICollection<Person> Persons { get; set; }
        public virtual ICollection<ChatMessage> Messages { get; set; }
    }
}