﻿using System;

namespace MtiApi.Entity
{
    public class ChatMessage
    {
        public int Id { get; set; }
        public int ChatId { get; set; }
        public int SenderId { get; set; }
        public string Message { get; set; }
        public DateTime CreatedAt { get; set; }
        
        public virtual Person Sender { get; set; }
        public virtual Chat Chat { get; set; }
    }
}