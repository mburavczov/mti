﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MtiApi.Entity.Configuration
{
    public class ProjectConfiguration : IEntityTypeConfiguration<Project>
    {
        public void Configure(EntityTypeBuilder<Project> builder)
        {
            builder.HasOne(x => x.Author)
                .WithMany(x => x.Projects)
                .HasForeignKey(x => x.PersonId);

            builder.HasMany(x => x.Tags)
                .WithMany(x => x.Projects)
                .UsingEntity(x => x.ToTable("ProjectTag"));
        }
    }
}