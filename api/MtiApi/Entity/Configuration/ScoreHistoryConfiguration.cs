﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MtiApi.Entity.Configuration
{
    public class ScoreHistoryConfiguration : IEntityTypeConfiguration<ScoreHistory>
    {
        public void Configure(EntityTypeBuilder<ScoreHistory> builder)
        {
            builder.HasOne(x => x.Project)
                .WithMany(x => x.ScoreHistories)
                .HasForeignKey(x => x.ProjectId);
        }
    }
}