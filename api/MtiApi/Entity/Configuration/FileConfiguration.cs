﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MtiApi.Entity.Configuration
{
    public class FileConfiguration : IEntityTypeConfiguration<File>
    {
        public void Configure(EntityTypeBuilder<File> builder)
        {
            builder.HasOne(x => x.Person)
                .WithMany(x => x.Files)
                .HasForeignKey(x => x.PersonId);

            builder.HasOne(x => x.Project)
                .WithMany(x => x.Files)
                .HasForeignKey(x => x.ProjectId);
        }
    }
}