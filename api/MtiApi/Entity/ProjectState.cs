namespace MtiApi.Entity
{
    public enum ProjectState
    {
        Screening =  0,
        Scoring = 1,
        ExpertCouncil = 2,
        AccelerationProgram = 3,
        PilotingProgram = 4,
        Investing = 5
    }
}