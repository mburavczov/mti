﻿using System.ComponentModel.DataAnnotations.Schema;

namespace MtiApi.Entity
{
    public class ScoreHistory
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public ProjectState ProjectState { get; set; }
        public double Value { get; set; }
        
        public virtual Project Project { get; set; }
    }
}