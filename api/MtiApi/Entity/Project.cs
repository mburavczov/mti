﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MtiApi.Entity
{
    public class Project
    {
        public Project()
        {
            Tags = new List<Tag>();
        }
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public int PersonId { get; set; }
        [Required]
        public ProjectState State { get; set; }
        [Required]
        public string AuthorName { get; set; }
        [Required]
        public string Status { get; set; }
        [Required]
        public string ShortDescription { get; set; }
        [Required]
        public string UseCase { get; set; }
        [Required]
        public string Benefits { get; set; }
        public string TargetOrganization { get; set; }
        public string Vision { get; set; }
        public string NeedCertification { get; set; }
        [Required]
        public string ContactName { get; set; }
        public string ContactPosition { get; set; }
        [Required]
        public string ContactPhone { get; set; }
        [Required]
        public string ContactEmail { get; set; }
        public string LegalName { get; set; }
        public string INN { get; set; }
        public string EmployeeCount { get; set; }
        public string Site { get; set; }
        public string Source { get; set; }
        public string PresentationLink { get; set; }
        public double Rating { get; set; }
        
        public virtual Person Author { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
        public virtual ICollection<File> Files { get; set; }
        public virtual ICollection<ScoreHistory> ScoreHistories { get; set; }
    }
}