﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MtiApi.Entity
{
    public class Tag
    {
        public Tag()
        {
            Problems = new List<Problem>();
            Projects = new List<Project>();
        }
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        
        public virtual ICollection<Problem> Problems { get; set; }
        public virtual ICollection<Project> Projects { get; set; }
    }
}