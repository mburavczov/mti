﻿using System.Collections.Generic;

namespace MtiApi.Entity
{
    public class Problem
    {
        public Problem()
        {
            Tags = new List<Tag>();
        }
        
        public int Id { get; set; }
        public int PersonId { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string Aftereffect { get; set; }
        public string Reason { get; set; }
        public string AffectedTo { get; set; }
        public string Period { get; set; }
        public string AttemptToSolve { get; set; }
        public string Contacts { get; set; }
        
        public virtual Person Author { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
    }
}