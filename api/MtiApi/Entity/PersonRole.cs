﻿namespace MtiApi.Entity
{
    public enum PersonRole
    {
        Admin = 1,
        Customer = 2,
        Seller = 3
    }

    public static class RoleExtension
    {
        public static string Name(this PersonRole personRole)
        {
            return personRole switch
            {
                PersonRole.Admin => "admin",
                PersonRole.Customer => "customer",
                PersonRole.Seller => "seller",
                _ => ""
            };
        }
    }
}