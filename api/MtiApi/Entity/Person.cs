﻿using System.Collections.Generic;

namespace MtiApi.Entity
{
    public class Person
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public PersonRole Role { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Phone { get; set; }
        public string About { get; set; }
        public string AvatarPath { get; set; }
        public long TelegramChatId { get; set; }
        
        public virtual ICollection<Chat> Chats { get; set; }
        public virtual ICollection<ChatMessage> ChatMessages { get; set; }
        public virtual ICollection<Problem> Problems { get; set; }
        public virtual ICollection<Project> Projects { get; set; }
        public virtual ICollection<File> Files { get; set; }
    }
}