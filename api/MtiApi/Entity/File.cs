﻿using System.ComponentModel.DataAnnotations.Schema;

namespace MtiApi.Entity
{
    public class File
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int PersonId { get; set; }
        public int ProjectId { get; set; }
        public string Path { get; set; }
        
        public virtual Person Person { get; set; }
        public virtual Project Project { get; set; }
    }
}