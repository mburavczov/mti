﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MtiApi.Migrations
{
    public partial class fix_project : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PresentationLink",
                table: "Projects",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Source",
                table: "Projects",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PresentationLink",
                table: "Projects");

            migrationBuilder.DropColumn(
                name: "Source",
                table: "Projects");
        }
    }
}
