﻿using System.Linq;
using MtiApi.Entity;

namespace MtiApi.Repository
{
    public class ScoreHistoryRepository : BaseRepository<ScoreHistory>
    {
        public ScoreHistoryRepository(MainContext context) : base(context)
        {
        }

        public double TotalScore(int projectId)
        {
            var items = DbSet.Where(x => x.ProjectId == projectId).ToList();
            return items.Sum(x => x.Value) / items.Count;
        }
    }
}