﻿using MtiApi.Entity;

namespace MtiApi.Repository
{
    public class FileRepository : BaseRepository<File>
    {
        public FileRepository(MainContext context) : base(context)
        {
        }
    }
}