﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using MtiApi.Entity;

namespace MtiApi.Repository
{
    public class BaseRepository<TEntity> : BaseRepository<TEntity, MainContext> where TEntity : class
    {
        public BaseRepository(MainContext context) 
            : base(context) { }
    }
    
    public class BaseRepository<TEntity, TContext> 
        where TEntity : class
        where TContext : DbContext
    {
        protected TContext Context { get; private set; }
        protected DbSet<TEntity> DbSet { get; private set; }

        
        public BaseRepository(TContext context)
        {
            Context = context;
            DbSet = context.Set<TEntity>();
        }
        
        public TEntity Add(TEntity entity)
        {
            entity = DbSet.Add(entity).Entity;
            Context.SaveChanges();

            return entity;
        }
    }
}