﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using MtiApi.Entity;

namespace MtiApi.Repository
{
    public class ProjectRepository : BaseRepository<Project>
    {
        public ProjectRepository(MainContext context) : base(context)
        {
        }

        public void SetState(int projectId, ProjectState state)
        {
            var p = DbSet.FirstOrDefault(x => x.Id == projectId);
            if (p == null)
            {
                return;
            }
            
            p.State = state;
            DbSet.Update(p);
            Context.SaveChanges();
        }

        public Project GetById(int id)
        {
            return DbSet.Include(x => x.ScoreHistories).FirstOrDefault(x => x.Id == id);
        }

        public List<Project> GetByIds(int[] ids)
        {
            return DbSet.Include(x => x.ScoreHistories).Where(x => ids.Contains(x.Id)).OrderByDescending(x => x.Rating).ToList();
        }

        public List<Project> GetByPersonId(int id)
        {
            return DbSet.Include(x => x.ScoreHistories).OrderByDescending(x => x.Rating).Where(x => x.PersonId == id).ToList();
        }

        public void Update(Project project)
        {
            DbSet.Update(project);
            Context.SaveChanges();
        }
        
        public List<Project> GetAll()
        {
            return DbSet.Include(x => x.ScoreHistories).OrderByDescending(x => x.Rating).ToList();
        }
    }
}