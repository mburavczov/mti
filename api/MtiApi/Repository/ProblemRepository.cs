﻿using System.Collections.Generic;
using System.Linq;
using MtiApi.Entity;

namespace MtiApi.Repository
{
    public class ProblemRepository : BaseRepository<Problem>
    {
        public ProblemRepository(MainContext context) : base(context)
        {
        }

        public List<Problem> GetByPersonId(int personId)
        {
            return DbSet.Where(x => x.PersonId == personId).ToList();
        }


        public List<Problem> GetByIds(int[] ids)
        {
            return DbSet.Where(x => ids.Contains(x.Id)).ToList();
        }

        public List<Problem> GetAll()
        {
            return DbSet.ToList();
        }
    }
}