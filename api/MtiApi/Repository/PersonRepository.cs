﻿using System.Collections.Generic;
using System.Linq;
using MtiApi.Entity;

namespace MtiApi.Repository
{
    public class PersonRepository : BaseRepository<Person>
    {
        public PersonRepository(MainContext context) : base(context)
        {
        }

        
        public Person Get(string login)
        {
            return DbSet.FirstOrDefault(x => x.Login == login);
        }
        
        public List<Person> GetAll()
        {
            return DbSet.ToList();
        }

        public void Update(Person person)
        {
            DbSet.Update(person);
            Context.SaveChanges();
        }

        public Person GetById(int id)
        {
            return DbSet.FirstOrDefault(x => x.Id == id);
        }

        public List<Person> GetWithTelegramId()
        {
            return DbSet.Where(x => x.TelegramChatId > 0).ToList();
        }

        public List<Person> GetByIds(int[] ids)
        {
            return DbSet.Where(x => ids.Contains(x.Id)).ToList();
        }

        public void SetTelegramChatId(int personId, long chatId)
        {
            var p = DbSet.FirstOrDefault(x => x.Id == personId);
            if (p == null)
            {
                return;
            }

            p.TelegramChatId = chatId;
            DbSet.Update(p);
            Context.SaveChanges();
        }
    }
}