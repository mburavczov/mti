﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Security.Claims;
using System.Threading.Tasks;
using Dropbox.Api;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using MtiApi.ApiModel.Request.Account;
using MtiApi.ApiModel.Response.Account;
using MtiApi.ApiModel.Response.Profile;
using MtiApi.Entity;
using MtiApi.Repository;
using MtiApi.Services;
using MtiApi.Settings;

namespace MtiApi.Controllers
{
    /// <summary>
    /// Methods for register, authentication and authorization
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        private readonly PersonRepository _personRepository;
        private readonly DropboxService _dropboxService;

        public AccountController(PersonRepository personRepository, DropboxService dropboxService)
        {
            _personRepository = personRepository;
            _dropboxService = dropboxService;
        }

        /// <summary>
        /// Get JWT token by login and password
        /// </summary>
        /// <returns>JWT token</returns>
        /// <response code="200">Ok</response>
        /// <response code="400">Invalid username or password</response>
        [HttpPost("Token")]
        [ProducesResponseType(typeof(TokenResponse), 200)]
        public IActionResult Token([FromBody] TokenRequest data)
        {
            var person = _personRepository.Get(data.Login);
            if (person == null || !BCrypt.Net.BCrypt.Verify(data.Pass, person.Password))
            {
                return BadRequest(new { errorText = "Invalid username or password." });
            }

            return Json(new TokenResponse
            {
                Token = GetToken(person)
            });
        }

        /// <summary>
        /// Register new account
        /// </summary>
        /// <returns>Token</returns>
        /// <response code="200">Ok</response>
        [HttpPost("Register")]
        [ProducesResponseType(typeof(RegisterResponse), 200)]
        public IActionResult Register([FromBody] RegisterRequest data)
        {
            var person = new Person
            {
                Login = data.Login,
                Password = BCrypt.Net.BCrypt.HashPassword(data.Pass),
                Name = data.Name,
                Surname = data.Surname,
                Phone = data.Phone,
                Role = data.Type == PersonType.Seller ? PersonRole.Seller : PersonRole.Customer,
                About = data.About
            };
            _personRepository.Add(person);

            return Ok(new RegisterResponse
            {
                Token = GetToken(person)
            });
        }
        
        /// <summary>
        /// Get profile info
        /// </summary>
        /// <param name="personId">If not specified, the current id is used</param>
        /// <response code="404">Person not found</response>
        [Authorize]
        [HttpGet("Info")]
        [ProducesResponseType(typeof(PersonItem), 200)]
        public async Task<IActionResult> Info(int? personId)
        {
            if (!personId.HasValue)
            {
                personId = int.Parse(User?.FindFirst(x => x.Type == Constants.ClaimTypeUserId)?.Value);
            }

            var person = _personRepository.GetById(personId.Value);
            if (person == null)
            {
                return NotFound();
            }

            var info = await MapPerson(person);

            return Ok(info);
        }

        /// <summary>
        /// Update profile avatar
        /// </summary>
        /// <response code="404">Person not found</response>
        /// <response code="400">Zero or more than 1 files in request</response>
        [Authorize]
        [HttpPost("UpdateAvatar")]
        [ProducesResponseType(typeof(UpdateAvatarResponse), 200)]
        public async Task<IActionResult> UpdateAvatar()
        {
            var personId = int.Parse(User?.FindFirst(x => x.Type == Constants.ClaimTypeUserId)?.Value);
            var person = _personRepository.GetById(personId);
            if (person == null)
            {
                return NotFound();
            }

            if (Request.Form.Files.Count != 1)
            {
                return BadRequest();
            }

            var filePath = $"/mti/{personId}/{Guid.NewGuid().ToString()}.jpg";
            using (var dbx = new DropboxClient(Environment.GetEnvironmentVariable("DROPBOX_TOKEN")))
            {
                using (var fs = Request.Form.Files[0].OpenReadStream())
                {
                    var bytes = ReadFully(fs);
                    await _dropboxService.Upload(bytes, filePath);
                }

                person.AvatarPath = filePath;
                _personRepository.Update(person);
                var path = await dbx.Files.GetTemporaryLinkAsync(filePath);

                return Ok(new UpdateAvatarResponse
                {
                    Link = path.Link
                });
            }
        }

        [HttpPost("RegisterTelegram")]
        public IActionResult RegisterTelegram(RegisterTelegramRequest model)
        {
            _personRepository.SetTelegramChatId(model.PersonId, model.ChatId);
            return Ok();
        }

        private string GetToken(Person person)
        {
            var claims = new List<Claim>
            {
                new Claim(Constants.ClaimTypeName, person.Name),
                new Claim(Constants.ClaimTypeRole, ((int)person.Role).ToString()),
                new Claim(Constants.ClaimTypeLogin, person.Login),
                new Claim(Constants.ClaimTypeUserId, person.Id.ToString())
            };
            claims.AddRange(GetIdentity(person).Claims);
            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                issuer: AuthOptions.ISSUER,
                audience: AuthOptions.AUDIENCE,
                notBefore: now,
                claims: claims,
                expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(),
                    SecurityAlgorithms.HmacSha256));

            return new JwtSecurityTokenHandler().WriteToken(jwt);
        }
        
        private ClaimsIdentity GetIdentity(Person person)
        {
            if (person != null)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, person.Login),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, person.Role.Name())
                };
                ClaimsIdentity claimsIdentity =
                    new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                        ClaimsIdentity.DefaultRoleClaimType);
                return claimsIdentity;
            }

            return null;
        }
        
        private async Task<PersonItem> MapPerson(Person person)
        {
            var info = new PersonItem
            {
                Id = person.Id,
                Login = person.Login,
                Role = (int)person.Role,
                Name = person.Name,
                Surname = person.Surname,
                Phone = person.Phone,
                About = person.About,
            };

            if (!string.IsNullOrEmpty(person.AvatarPath))
            {
                info.AvatarPath = await _dropboxService.GetTemporaryLink(person.AvatarPath);
            }

            return info;
        }
        
        private static byte[] ReadFully(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }
    }
}