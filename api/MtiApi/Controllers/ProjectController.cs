﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MtiApi.ApiModel.Elastic;
using MtiApi.ApiModel.Request.Project;
using MtiApi.ApiModel.Response.Project;
using MtiApi.Entity;
using MtiApi.Repository;
using MtiApi.Services;
using MtiApi.Settings;
using Nest;
using Telegram.Bot;

namespace MtiApi.Controllers
{
    /// <summary>
    /// Methods for create, manage and search projects
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectController : Controller
    {
        private readonly ProblemRepository _problemRepository;
        private readonly ProjectRepository _projectRepository;
        private readonly IElasticClient _elasticClient;
        private readonly PersonRepository _personRepository;
        private readonly ITelegramBotClient _telegramBotClient; 
        private readonly DropboxService _dropboxService;
        private readonly FileRepository _fileRepository;
        private readonly ScoreHistoryRepository _scoreHistoryRepository;

        
        public ProjectController(ProblemRepository problemRepository, 
            ProjectRepository projectRepository, 
            IElasticClient elasticClient, 
            PersonRepository personRepository, 
            ITelegramBotClient telegramBotClient, 
            DropboxService dropboxService, 
            FileRepository fileRepository, 
            ScoreHistoryRepository scoreHistoryRepository)
        {
            _problemRepository = problemRepository;
            _projectRepository = projectRepository;
            _elasticClient = elasticClient;
            _personRepository = personRepository;
            _telegramBotClient = telegramBotClient;
            _dropboxService = dropboxService;
            _fileRepository = fileRepository;
            _scoreHistoryRepository = scoreHistoryRepository;
        }

        
        [HttpPost("AddProblem")]
        [Authorize(Roles = "customer,admin")]
        public IActionResult AddProblem(AddProblemRequest model)
        {
            var personId = int.Parse(User?.FindFirst(x => x.Type == Constants.ClaimTypeUserId)?.Value);
            var problem = new Problem
            {
                PersonId = personId,
                ShortDescription = model.ShortDescription,
                LongDescription = model.LongDescription,
                Aftereffect = model.Aftereffect,
                Reason = model.Reason,
                AffectedTo = model.AffectedTo,
                Period = model.Period,
                AttemptToSolve = model.AttemptToSolve,
                Contacts = model.Contacts,
                Tags = model.Tags?.Select(x => new Tag { Name = x }).ToList()
            };
            _problemRepository.Add(problem);

            _elasticClient.IndexDocument(new ProblemModel
            {
                Description = $"{problem.LongDescription} {problem.Aftereffect} {problem.Reason} {problem.AffectedTo}",
                ProblemId = problem.Id
            });
            
            return Ok();
        }

        [HttpGet("GetProblemsByPersonId")]
        [Authorize]
        public IActionResult GetProblemsByPersonId(int personId)
        {
            var problems = _problemRepository.GetByPersonId(personId).Select(MapProblem).ToList();

            return Ok(problems);
        }

        [HttpGet("GetProblems")]
        [Authorize]
        public IActionResult GetProblems()
        {
            var problems = _problemRepository.GetAll().Select(MapProblem).ToList();

            return Ok(problems);
        }

        [HttpPost("AddProject")]
        [Authorize(Roles = "seller")]
        public async Task<IActionResult> AddProject(AddProjectRequest model)
        {
            var personId = int.Parse(User?.FindFirst(x => x.Type == Constants.ClaimTypeUserId)?.Value);
            var project = new Project
            {
                PersonId = personId,
                State = ProjectState.Screening,
                AuthorName = model.AuthorName,
                Status = model.Status,
                ShortDescription = model.ShortDescription,
                UseCase = model.UseCase,
                Benefits = model.Benefits,
                TargetOrganization = model.TargetOrganization,
                Vision = model.Vision,
                NeedCertification = model.NeedCertification,
                ContactName = model.ContactName,
                ContactPosition = model.ContactPosition,
                ContactPhone = model.ContactPhone,
                ContactEmail = model.ContactEmail,
                LegalName = model.LegalName,
                INN = model.INN,
                EmployeeCount = model.EmployeeCount,
                Site = model.Site,
                Source = model.Source,
                PresentationLink = model.PresentationLink,
                Tags = model.Tags?.Select(x => new Tag{Name = x}).ToList()
            };
            _projectRepository.Add(project);

            await _elasticClient.IndexDocumentAsync(new ProjectModel
            {
                Description = $"{project.ShortDescription} {project.UseCase} {project.Benefits} {project.Vision}",
                ProjectId = project.Id
            });

            var persons = _personRepository.GetWithTelegramId();
            foreach (var person in persons)
            {
                await _telegramBotClient.SendTextMessageAsync(person.TelegramChatId, 
                    $"На портале появился новый проект.\n«{project.ShortDescription}»\n\nПодробности в личном кабинете.");
            }

            return Ok();
        }

        [HttpPost("SetProjectState")]
        [Authorize(Roles = "admin")]
        public IActionResult SetProjectState(SetProjectStateRequest model)
        {
            _projectRepository.SetState(model.ProjectId, model.State);

            return Ok();
        }

        [HttpPost("NextStep")]
        [Authorize(Roles = "admin")]
        public IActionResult NextStep(NextStepRequest model)
        {
            var project = _projectRepository.GetById(model.ProjectId);
            if (project == null)
            {
                return BadRequest();
            }

            if (project.State == ProjectState.Investing)
            {
                return BadRequest("project in investing state");
            }

            _scoreHistoryRepository.Add(new ScoreHistory
            {
                ProjectId = model.ProjectId,
                Value = model.Score,
                ProjectState = project.State
            });

            project.State = (ProjectState)((int)project.State + 1);
            project.Rating = _scoreHistoryRepository.TotalScore(project.Id);
            _projectRepository.Update(project);

            return Ok();
        }

        [HttpGet("Projects")]
        [Authorize(Roles = "customer,admin")]
        public IActionResult Projects(ProjectState? state)
        {
            var projects = _projectRepository.GetAll()
                .Where(x => !state.HasValue || x.State == state)
                .Select(MapProject)
                .ToList();

            return Ok(projects);
        }

        [HttpGet("UserProjects")]
        [Authorize]
        public IActionResult UserProjects()
        {
            var personId = int.Parse(User?.FindFirst(x => x.Type == Constants.ClaimTypeUserId)?.Value);
            var projects = _projectRepository.GetByPersonId(personId).Select(MapProject).ToList();

            return Ok(projects);
        }

        [HttpGet("Projects/{id:int}")]
        [Authorize]
        public IActionResult Projects(int id)
        {
            var project = _projectRepository.GetById(id);
            if (project == null)
            {
                return NotFound();
            }
            
            return Ok(MapProject(project));
        }

        [HttpGet("SearchProject")]
        [Authorize]
        public IActionResult SearchProject(string query, ProjectState? state)
        {
            var searchRes = _elasticClient.Search<ProjectModel>(x =>
                x.Query(q =>
                    q.MultiMatch(m => m.Query(query).Fuzziness(Fuzziness.Auto))
                        ));

            var projectIds = searchRes.Hits.Select(x => x.Source.ProjectId).ToArray();

            var projects = _projectRepository
                .GetByIds(projectIds)
                .Where(x => !state.HasValue || x.State == state)
                .Select(MapProject)
                .ToList();
            return Ok(projects);
        }
        
        [HttpGet("SearchProblems")]
        [Authorize]
        public IActionResult SearchProblems(string query)
        {
            var searchRes = _elasticClient.Search<ProblemModel>(x =>
                x.Query(q =>
                    q.Fuzzy(t => t
                        .Value(query)
                        .Fuzziness(Fuzziness.Auto)
                    )));

            var ids = searchRes.Hits.Select(x => x.Source.ProblemId).ToArray();

            var items = _problemRepository.GetByIds(ids).Select(MapProblem).ToList();
            return Ok(items);
        }
        
        [Authorize]
        [HttpPost("UploadFile")]
        public async Task<IActionResult> UploadFile(int projectId)
        {
            var personId = int.Parse(User?.FindFirst(x => x.Type == Constants.ClaimTypeUserId)?.Value);
            var person = _personRepository.GetById(personId);
            if (person == null)
            {
                return NotFound();
            }

            if (Request.Form.Files.Count == 0)
            {
                return BadRequest();
            }

            foreach (var file in Request.Form.Files)
            {
                var filePath = $"/mti/{personId}/{Guid.NewGuid().ToString()}{Path.GetExtension(file.Name)}";

                using (var fs = file.OpenReadStream())
                {
                    var bytes = ReadFully(fs);
                    await _dropboxService.Upload(bytes, filePath);
                }

                _fileRepository.Add(new Entity.File
                {
                    PersonId = personId,
                    ProjectId = projectId,
                    Path = filePath
                });
            }

            return Ok();
        }
        

        private ProblemItem MapProblem(Problem model)
        {
            return new ProblemItem
            {
                Id = model.Id,
                PersonId = model.PersonId,
                ShortDescription = model.ShortDescription,
                LongDescription = model.LongDescription,
                Aftereffect = model.Aftereffect,
                Reason = model.Reason,
                AffectedTo = model.AffectedTo,
                Period = model.Period,
                AttemptToSolve = model.AttemptToSolve,
                Contacts = model.Contacts,
            };
        }

        private ProjectItem MapProject(Project model)
        {
            return new ProjectItem
            {
                Id = model.Id,
                PersonId = model.PersonId,
                State = model.State,
                AuthorName = model.AuthorName,
                Status = model.Status,
                ShortDescription = model.ShortDescription,
                UseCase = model.UseCase,
                Benefits = model.Benefits,
                TargetOrganization = model.TargetOrganization,
                Vision = model.Vision,
                NeedCertification = model.NeedCertification,
                ContactName = model.ContactName,
                ContactPosition = model.ContactPosition,
                ContactPhone = model.ContactPhone,
                ContactEmail = model.ContactEmail,
                LegalName = model.LegalName,
                INN = model.INN,
                EmployeeCount = model.EmployeeCount,
                Site = model.Site,
                PresentationLink = model.PresentationLink,
                Source = model.Source,
                Rating = model.Rating,
                ScoreHistories = model.ScoreHistories?.Select(s => new ScoreHistoryItem
                {
                    Value = s.Value,
                    ProjectState = s.ProjectState
                }).ToList()
            };
        }
        
        private static byte[] ReadFully(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }
    }
}